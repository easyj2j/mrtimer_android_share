package work.johnmac.mrtimer;

import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import work.johnmac.mrtimer.model.Score;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static work.johnmac.mrtimer.utility.DateConverter.formatIntAsString;

public class ScoreFragmentTest {
    private static final String SELECTED_ITEM_4 = "FF Stone"; // index 3
    private static final String SELECTED_ITEM_12 = "Sun Tzu"; // index 11
    private static final String ZERO = "0.000";

    private static final int USER_ID = -99;
    private static final String USER_NAME = "Testy";
    private static final int REACTION = 123;
    private static final int MOTION = 234;
    private static final int RESPONSE = REACTION + MOTION;

    private static final Score NEWSCORE = new Score(USER_ID, USER_NAME, REACTION, MOTION, RESPONSE, System.currentTimeMillis());

    private MainActivity mainActivity;
    private ScoreFragment scoreFragment;

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() throws Exception {
        mainActivity = activityTestRule.getActivity();
        scoreFragment =
                (ScoreFragment)mainActivity.getSupportFragmentManager().findFragmentByTag(mainActivity.scoreFragmentTag);
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
        scoreFragment = null;
    }

    @Test
    public void scoreFragmentIsVisible() throws Exception {
        onView(withId(R.id.user_spinner)).check(matches(isDisplayed()));
        onView(withId(R.id.group_label)).check(matches(not(isDisplayed())));
    }

    @Test
    public void onSpinnerItemSelected() throws Exception {
        //onData(withValue(SELECTED_ITEM_12)).inAdapterView(withId(R.id.user_spinner)).perform(click());
        onView(withId(R.id.user_spinner)).perform(click());
        onData(is(SELECTED_ITEM_12)).perform(click());
        onView(withId(R.id.user_spinner)).check(matches(withSpinnerText(SELECTED_ITEM_12)));

        onView(withId(R.id.user_spinner)).perform(click());
        onData(is(SELECTED_ITEM_4)).perform(click());
        onView(withId(R.id.user_spinner)).check(matches(not(withSpinnerText(SELECTED_ITEM_12))));
        onView(withId(R.id.user_spinner)).check(matches(withSpinnerText(SELECTED_ITEM_4)));
    }

    @Test
    public void zeroScore() throws Throwable {
        activityTestRule.runOnUiThread(() -> {
                scoreFragment.updateScore(NEWSCORE);
                scoreFragment.zeroScore();
        });
        onView(withId(R.id.reaction_view)).check(matches(withText(ZERO)));
        onView(withId(R.id.motion_view)).check(matches(withText(ZERO)));
        onView(withId(R.id.response_view)).check(matches(withText(ZERO)));
    }

//    @UiThreadTest - hangs unless using CountingIdlingResource
    @Test
    public void updateScore() throws Throwable {// Exception {
        activityTestRule.runOnUiThread(() -> {
                scoreFragment.zeroScore();
                scoreFragment.updateScore(NEWSCORE);
        });
        onView(withId(R.id.reaction_view)).check(matches(withText(formatIntAsString(NEWSCORE.getReaction()))));
        onView(withId(R.id.motion_view)).check(matches(withText(formatIntAsString(NEWSCORE.getMotion()))));
        onView(withId(R.id.response_view)).check(matches(withText(formatIntAsString(NEWSCORE.getResponse()))));
    }
}