package work.johnmac.mrtimer.model;

import android.content.ContentValues;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static work.johnmac.mrtimer.model.SqlDbHelper.MOTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.NAME;
import static work.johnmac.mrtimer.model.SqlDbHelper.REACTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.RESPONSE;
import static work.johnmac.mrtimer.model.SqlDbHelper.RETRIEVED;

/**
 * Score instrumented unit tests.
 */
// TODO: test Parcelable: http://www.kevinrschultz.com/blog/2014/03/01/how-not-to-test-androids-parcelable-interface/
@RunWith(AndroidJUnit4.class)
public class ScoreInstrumentedTest {
    private static final int USER_ID = -99;
    private static final String USER_NAME = "Testy";
    private static final int REACTION_TIME = 414;
    private static final int MOTION_TIME = 316;
    private static final int RESPONSE_TIME = 730;

    @Test
    public void testScoreToContentValues() {
        Score score1 = new Score(USER_ID, USER_NAME, REACTION_TIME, MOTION_TIME, RESPONSE_TIME, System.currentTimeMillis());

        ContentValues cv = score1.toContentValues();
        Score score = new Score(USER_ID,
                cv.getAsString(NAME),
                cv.getAsInteger(REACTION),
                cv.getAsInteger(MOTION),
                cv.getAsInteger(RESPONSE),
                cv.getAsLong(RETRIEVED));
        assertEquals(score1, score);
    }
}