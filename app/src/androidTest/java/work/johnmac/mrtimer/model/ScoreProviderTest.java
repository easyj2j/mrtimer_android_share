package work.johnmac.mrtimer.model;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.ProviderTestCase2;
import android.test.RenamingDelegatingContext;
import android.test.mock.MockContentResolver;
import android.test.mock.MockContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;
import static work.johnmac.mrtimer.model.ScoreProvider.SCORES_URI;
import static work.johnmac.mrtimer.model.ScoreProvider.SCORE_URI;
import static work.johnmac.mrtimer.model.SqlDbHelper.NAME;
import static work.johnmac.mrtimer.model.SqlDbHelper.MOTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.REACTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.RESPONSE;
import static work.johnmac.mrtimer.model.SqlDbHelper.RETRIEVED;
import static work.johnmac.mrtimer.model.SqlDbHelper.RETRIEVED_TIME;
import static work.johnmac.mrtimer.utility.DateConverter.DISPLAY_DATE_FORMAT;
import static work.johnmac.mrtimer.utility.DateConverter.setRawTime;

@RunWith(AndroidJUnit4.class)
public class ScoreProviderTest extends ProviderTestCase2<ScoreProvider> {
    // Retrieved in setup().
    private ContentProvider provider;
    private MockContentResolver mockResolver;

    public ScoreProviderTest() {
        super(ScoreProvider.class, ScoreProvider.AUTHORITY);
    }

    @Before
    @Override
    public void setUp() throws Exception {
        super.setUp();
        provider = getProvider();
        mockResolver = getMockContentResolver();
    }

    @After
    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testIsIdAnInt() {
        boolean result = ((ScoreProvider)provider).isIdAPositiveLong("123");
        assertTrue(result);
    }

    @Test
    public void testIsIdAnInt2() {
        boolean result = ((ScoreProvider)provider).isIdAPositiveLong("nan");
        assertFalse(result);
    }

    @Test
    public void testGetTypeScores() {
        String result = provider.getType(ScoreProvider.SCORES_URI);
        assertEquals(ScoreProvider.MIME_SCORES, result);
    }

    @Test
    public void testGetTypeScore() {
        String result = provider.getType(SCORE_URI);
        assertNotEquals(ScoreProvider.MIME_SCORE, result);
    }

    @Test
    public void testGetTypeScoreNumber() {
        String result = provider.getType(Uri.withAppendedPath(SCORE_URI, "123"));
        assertEquals(ScoreProvider.MIME_SCORE, result);
    }

    @Test
    public void testGetTypeUnsupported() {
        String result = provider.getType(Uri.parse("badUri"));
        assertEquals(ScoreProvider.UNSUPPORTED_URI, result);
    }

    @Test
    public void query() throws Exception {

    }

    @Test
    public void delete() throws Exception {

    }

//    @Ignore("Runs against real DB; add mock")
    @Test
    public void testInsertAndQuery() {
        // No difference in using mockResolver or provider.
        Cursor cursor = provider.query(SCORES_URI, null, null, null, null);
        assertNotNull(cursor);

        int initialCount = cursor.getCount();
        cursor.close();

        ContentValues cv = createMockContentValues();
        long id1 = ContentUris.parseId(provider.insert(SCORE_URI, cv));
        assertTrue(0 < id1);

        long id2 = ContentUris.parseId(provider.insert(SCORE_URI, cv));
        assertTrue(id1 < id2);

        cursor = provider.query(SCORES_URI, null, null, null, null);
        assertEquals(2, cursor.getCount() - initialCount);
        cursor.close();
    }

    @Test
    public void update() throws Exception {
    }

    private ContentValues createMockContentValues() {
        ContentValues cv = new ContentValues(6);
        cv.put(NAME, "Testy");
        cv.put(REACTION, 123);
        cv.put(MOTION, 456);
        cv.put(RESPONSE, 579);
        cv.put(RETRIEVED, setRawTime());
        cv.put(RETRIEVED_TIME, DISPLAY_DATE_FORMAT.format(setRawTime()));
        return cv;
    }
}