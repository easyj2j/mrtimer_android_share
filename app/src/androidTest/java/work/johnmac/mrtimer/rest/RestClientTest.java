package work.johnmac.mrtimer.rest;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.mockwebserver.MockWebServer;
import work.johnmac.mrtimer.App;
import work.johnmac.mrtimer.MainActivity;
import work.johnmac.mrtimer.model.Score;

import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

/**
 * Azure webapp startup time is in secs; TODO: initial tests will fail unless pre-ping azure app.
 * Do as part of check for network connection.
 *
 * Azure call latency ~ 400 ms -> sleep(1000) afterwards.
 */

@RunWith(MockitoJUnitRunner.class)
public class RestClientTest {
    //private static final String HOST_URL = "http://mrtimer.azurewebsites.net/";
    private static final String HOST_URL = "http://easyj2j-001-site1.ftempurl.com/";
    private static final int USER_ID = 0;
    private static final String USER_NAME = "Testy";
    private static final int REACTION_TIME = 414;
    private static final int MOTION_TIME = 316;
    private static final int RESPONSE_TIME = 730;

    private MainActivity mainActivity;
    private MockWebServer mockServer;

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Rule
    public MockWebServer mockBackend = new MockWebServer();

    @Test
    public void testMockBackend() {
        App app = (App)mainActivity.getApplication();
        RestClient restClient = app.getRestClient();
    }

    @Before
    public void setUp() {
        mainActivity = activityTestRule.getActivity();
    }

    @After
    public void tearDown() {
        mainActivity = null;
    }

    @Ignore("Runs against real ReST server; add mock")
    @Test
    public void testInsertRetrieveAndDelete() throws InterruptedException {
        App app = (App)mainActivity.getApplication();
        RestClient restClient = app.getRestClient();

        Score newScore  = new Score(USER_ID, USER_NAME, REACTION_TIME, MOTION_TIME, RESPONSE_TIME, System.currentTimeMillis());

        restClient.insertScore(newScore);
        assertTrue(waitForNewScoreId(restClient));
        Log.d("RestClientTest", "newScoreId: " + restClient.newScoreId);

        restClient.retrieveScoreGson(restClient.newScoreId);
        waitForRetrievedScore(restClient);
        Log.d("RestClientTest", "retrievedScore: " + restClient.retrievedScore);
        assertEquals(newScore, restClient.retrievedScore);

        restClient.deleteScore(restClient.newScoreId);
        Thread.sleep(1000);
        assertNull(restClient.retrievedScore);

        restClient.retrieveScoreGson(restClient.newScoreId);
        // Expected logcat output:
        // E/RestClient: retrieveScoreGson error, HTTP: 404
        waitForRetrievedScore(restClient);
        assertNull(restClient.retrievedScore);
    }

    // Polls for change from returned scoreId
    private boolean waitForNewScoreId(RestClient restClient) throws InterruptedException {
        for(int i = 0; i < 50; i++) {
            Thread.sleep(100);
            if(restClient.newScoreId != -1) {
                Log.d("RestClientTest", "waitForNewScoreId, elapsed time: " + i * 100 + " msec");
                return true;
            }
        }
        return false;
    }

    // Polls for change from returned score
    private void waitForRetrievedScore(RestClient restClient) throws InterruptedException {
        for(int i = 0; i < 50; i++) {
            Thread.sleep(100);
            if(restClient.retrievedScore != null) {
                Log.d("RestClientTest", "waitForRetrievedScore, elapsed time: " + i * 100 + " msec");
                return;
            }
        }
    }

    private String getStringFromAssetFile(String fileName) {
        String json = null;
        Context testContext = InstrumentationRegistry.getInstrumentation().getContext();
        InputStream stream;
        BufferedReader reader = null;
        try {
            stream = testContext.getResources().getAssets().open(fileName);
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();
            while ((json = reader.readLine()) != null) {
                builder.append(json).append("\n");
            }
            json = builder.toString();
        } catch (IOException e) {
            //Log.e(TAG, "getStringFromAssetFile: ", e);
        }
        finally {
            if(reader != null) {
                try {
                    // BufferedReader.close calls InputStreamReader.close calls InputStream.close
                    reader.close();
                } catch (IOException e) {
                    //Log.e(TAG, "getStringFromAssetFile: ", e);
                }
            }
        }
        return json;
    }
}
