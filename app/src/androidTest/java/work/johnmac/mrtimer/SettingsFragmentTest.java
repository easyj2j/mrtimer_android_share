package work.johnmac.mrtimer;

import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

public class SettingsFragmentTest {
    private MainActivity mainActivity;
    private SettingsFragment settingsFragment;

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() throws Exception {
        mainActivity = activityTestRule.getActivity();
        settingsFragment =
                (SettingsFragment)mainActivity.getSupportFragmentManager().findFragmentByTag(mainActivity.settingsFragmentTag);
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
        settingsFragment = null;
    }

    @Test
    public void settingsFragmentIsVisible() throws Exception {
        onView(withId(R.id.container)).perform(swipeLeft()).perform(swipeLeft());

        // scores_list_header extends over the complete screen width,
        // whereas user_spinner & group_label extend over a small central area.
        // Instead of code to wait for the flings to finish, just test scores_list_header.isCompletelyDisplayed.
        onView(withId(R.id.user_spinner)).check(matches(not(isDisplayed())));
        onView(withId(R.id.scores_list_header)).check(matches(not(isCompletelyDisplayed())));
        onView(withId(R.id.group_label)).check(matches(isDisplayed()));
    }
}