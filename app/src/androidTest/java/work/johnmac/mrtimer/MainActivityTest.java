package work.johnmac.mrtimer;

import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

public class MainActivityTest {
    private MainActivity mainActivity;

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    @Before
    public void setUp() throws Exception {
        mainActivity = activityTestRule.getActivity();
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
    }

    @Test
    public void testLaunch() {
        onView(withId(R.id.reaction_label)).check(matches(isDisplayed()));
    }

    @Test
    public void testTabs() {
        // Tabs are displayed in UPPERCASE but are really Title case strings.
        onView(allOf(isDescendantOfA(withId(R.id.sliding_tabs)), withText("Settings"))).perform(click());
        onView(withId(R.id.group_label)).check(matches(isDisplayed()));

        onView(allOf(isDescendantOfA(withId(R.id.sliding_tabs)), withText("Scores"))).perform(click());
        onView(withId(R.id.scores_list_header)).check(matches(isDisplayed()));
    }
}