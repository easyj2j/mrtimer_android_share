package work.johnmac.mrtimer;

import android.support.test.rule.ActivityTestRule;
import android.support.test.espresso.contrib.ViewPagerActions;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;

/**
 * Don't use ViewActions#scrollTo() to scroll the list as
 * Espresso.onData(org.hamcrest.Matcher)} handles scrolling.
 */
public class ScoresFragmentTest {
    private MainActivity mainActivity;
    private ScoresFragment scoresFragment;

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void setUp() throws Exception {
        mainActivity = activityTestRule.getActivity();
        scoresFragment =
                (ScoresFragment)mainActivity.getSupportFragmentManager().findFragmentByTag(mainActivity.scoresFragmentTag);
    }

    @After
    public void tearDown() throws Exception {
        mainActivity = null;
        scoresFragment = null;
    }

    @Test
    public void scoresFragmentIsVisible() throws Exception {
        // ViewPagerActions.scrollLeft() doesn't
        //onView(withId(R.id.container)).perform(ViewPagerActions.scrollLeft());
        onView(withId(R.id.container)).perform(swipeLeft());

        onView(withId(R.id.user_spinner)).check(matches(not(isDisplayed())));
        onView(withId(R.id.scores_list_header)).check(matches(isDisplayed()));
        onView(withId(R.id.group_label)).check(matches(not(isDisplayed())));
    }
}