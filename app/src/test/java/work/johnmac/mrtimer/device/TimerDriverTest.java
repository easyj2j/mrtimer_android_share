package work.johnmac.mrtimer.device;

import android.bluetooth.BluetoothDevice;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertArrayEquals;

import static work.johnmac.mrtimer.SettingsFragment.TRIGGER_BOTH_PREF;
import static work.johnmac.mrtimer.SettingsFragment.TRIGGER_LED_PREF;
import static work.johnmac.mrtimer.SettingsFragment.TRIGGER_SPEAKER_PREF;
import static work.johnmac.mrtimer.device.TimerDriver.DO_NOTHING;
import static work.johnmac.mrtimer.device.TimerDriver.TRIGGER_SPEAKER;
import static work.johnmac.mrtimer.device.TimerDriver.TRIGGER_LED;
import static work.johnmac.mrtimer.device.TimerDriver.TRIGGER_SOURCE;

/**
 * Unit tests for TimerDriver class.
 */
@RunWith(MockitoJUnitRunner.class)
public class TimerDriverTest {
    private TimerDriver timerDriver;
    private final byte[] expectedBuffer = new byte[4];

    @Mock
    private BluetoothDevice btDevice;

    @Before
    public void setup() throws Exception {
        // Disable android.util.Log calls
        TimerDriver.DEBUG = false;
        timerDriver = new TimerDriver();
        timerDriver.connectToTimer(btDevice);
        timerDriver.btConnectionState = BluetoothConnection.BT_STATE_CONNECTED;
        clearExpectedBuffer();
    }

    @Test
    public void setTriggerTest() throws Exception {
        timerDriver.clearBuffer();
        expectedBuffer[0] = TRIGGER_SOURCE;
        expectedBuffer[1] = TRIGGER_LED;
        timerDriver.setTrigger(TRIGGER_LED_PREF - 1);
        byte[] actualBuffer = timerDriver.getOutputBuffer();
        assertArrayEquals("actualBuffer: " + new String(actualBuffer), expectedBuffer, actualBuffer);

        timerDriver.clearBuffer();
        expectedBuffer[0] = TRIGGER_SOURCE;
        expectedBuffer[1] = TRIGGER_LED;
        timerDriver.setTrigger(TRIGGER_BOTH_PREF + 1);
        actualBuffer = timerDriver.getOutputBuffer();
        assertArrayEquals("actualBuffer: " + new String(actualBuffer), expectedBuffer, actualBuffer);

        timerDriver.clearBuffer();
        expectedBuffer[0] = TRIGGER_SOURCE;
        expectedBuffer[1] = TRIGGER_SPEAKER;
        timerDriver.setTrigger(TRIGGER_SPEAKER_PREF);
        actualBuffer = timerDriver.getOutputBuffer();
        assertArrayEquals("actualBuffer: " + new String(actualBuffer), expectedBuffer, actualBuffer);
    }
    // TODO: as above
    @Test public void requestOutputTest() throws Exception {}
    @Test public void latchOrResetDisplayTest() throws Exception {}
    @Test public void setTriggerDelayLimitsTest() throws Exception {}

    private void clearExpectedBuffer() {
        for(int i = 0; i < expectedBuffer.length; i++) {
            expectedBuffer[i] = DO_NOTHING;
        }
    }

    // Not unit testable; contained method calls are tested in their respective classes.
    //@Test public void connectToTimerTest() throws Exception {}
    //@Test public void closeBtConnectionTest() throws Exception {}
    //@Test public void setBtConnectionStateTest() throws Exception {}
    //@Test public void setRawScoreTest() throws Exception {}
    //@Test public void zeroScoreTest() throws Exception {}
}