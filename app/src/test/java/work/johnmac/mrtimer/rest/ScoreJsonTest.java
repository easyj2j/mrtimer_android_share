package work.johnmac.mrtimer.rest;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import work.johnmac.mrtimer.model.Score;

import static org.junit.Assert.*;

/**
 * Unit tests for ScoreJson class.
 */
@RunWith(MockitoJUnitRunner.class)
public class ScoreJsonTest {
    @Mock
    private JsonSerializationContext serializationContext;

    @Mock
    private JsonDeserializationContext deserializationContext;

    @Test
    public void roundTrip() throws Exception {
        Score score = new Score(99, "Testy", 123, 234, 357, System.currentTimeMillis());

        ScoreJson scoreJson = new ScoreJson();
        JsonElement jsonElement = scoreJson.serialize(score, Score.class, serializationContext);
        Score roundTripScore = scoreJson.deserialize(jsonElement, Score.class, deserializationContext);

        assertEquals(score, roundTripScore);
    }
}