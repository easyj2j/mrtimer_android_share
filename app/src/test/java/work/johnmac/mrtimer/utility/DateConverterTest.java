package work.johnmac.mrtimer.utility;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static work.johnmac.mrtimer.utility.DateConverter.MSEC_PRECISION_LOSS;
import static work.johnmac.mrtimer.utility.DateConverter.SEC_PRECISION_LOSS;
import static work.johnmac.mrtimer.utility.DateConverter.convertDisplayDateToRaw;
import static work.johnmac.mrtimer.utility.DateConverter.convertRawToDisplayDate;
import static work.johnmac.mrtimer.utility.DateConverter.convertRawToReSTDate;
import static work.johnmac.mrtimer.utility.DateConverter.convertReSTDateToRaw;
import static work.johnmac.mrtimer.utility.DateConverter.setRawTime;

/**
 * Unit tests for DateConverter class.
 */
public class DateConverterTest {
    private static final long LONGTIME = 1499372363000L;
    private static final String DISPLAYTIME = "07/06/17 01:19 PM";
    private static final String RESTTIME = "2017-07-06T13:19:23";

    @Before
    public void setup() {
        // Disable android.util.Log calls
        DateConverter.DEBUG = false;
    }

    @Test
    public void displayDateTest() {
        String displayTime = convertRawToDisplayDate(LONGTIME);
        assertEquals(DISPLAYTIME, displayTime);
    }

    @Test
    public void displayDateTest2() {
        long longTime = convertDisplayDateToRaw(DISPLAYTIME);
        assertEquals(LONGTIME/SEC_PRECISION_LOSS, longTime/SEC_PRECISION_LOSS);
    }

    @Test
    public void displayDateRoundTripTest() {
        long raw = setRawTime();
        String displayDate = convertRawToDisplayDate(raw);
        long rawRoundTrip = convertDisplayDateToRaw(displayDate);
        long diff = raw/SEC_PRECISION_LOSS - rawRoundTrip/SEC_PRECISION_LOSS;
        String displayDateRoundTrip = convertRawToDisplayDate(
                (rawRoundTrip/SEC_PRECISION_LOSS) * SEC_PRECISION_LOSS);
        assertEquals(0, diff);
        assertEquals(displayDate, displayDateRoundTrip);
    }

    @Test
    public void reSTDateTest() {
        String reSTTime = convertRawToReSTDate(LONGTIME);
        assertEquals(RESTTIME.replace("T", " "), reSTTime);
    }

    @Test
    public void reSTDateTest2() {
        long longTime = convertReSTDateToRaw(RESTTIME);
        assertEquals(LONGTIME/SEC_PRECISION_LOSS, longTime/SEC_PRECISION_LOSS);
    }

    @Test
    public void reSTDateRoundTripTest() {
        long raw = setRawTime();
        String reSTDate = convertRawToReSTDate(raw);
        long rawRoundTrip = convertReSTDateToRaw(reSTDate);
        long diff = raw/MSEC_PRECISION_LOSS - rawRoundTrip/MSEC_PRECISION_LOSS;
        String reSTDateRoundTrip = convertRawToReSTDate(
                (rawRoundTrip/MSEC_PRECISION_LOSS) * MSEC_PRECISION_LOSS);
        assertEquals(0, diff);
        assertEquals(reSTDate, reSTDateRoundTrip);
    }
}
