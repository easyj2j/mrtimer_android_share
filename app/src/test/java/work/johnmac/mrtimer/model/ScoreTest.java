package work.johnmac.mrtimer.model;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;
import static work.johnmac.mrtimer.model.Score.DEFAULT_LOCALID;
import static work.johnmac.mrtimer.model.Score.DEFAULT_LOCALNAME;

/**
 * Unit tests for Score class.
 */
public class ScoreTest {
    private static final int USER_ID = -99;
    private static final String USER_NAME = "Testy";

    // !&0414&0730\n
    private static final byte[] GOOD_DATA_PACKET = {
            0x21, 0x26, 0x30, 0x34, 0x31, 0x34, 0x26, 0x30, 0x37, 0x33, 0x30, 0xA };
    private static final int REACTION_TIME = 414;
    private static final int MOTION_TIME = 316;
    private static final int RESPONSE_TIME = 730;

    private static final byte[] BAD_DATA_PACKET = Arrays.copyOf(GOOD_DATA_PACKET, GOOD_DATA_PACKET.length);
    static {
        BAD_DATA_PACKET[2] = 0x20; // not an integer
    }

    private Score score1;
    private Score score2;
    private Score score3;
    private Score badScore;

    @Before
    public void setup() {
        // Disable android.util.Log calls
        Score.DEBUG = false;
        long retrievedTime = System.currentTimeMillis();
        score1 = new Score(DEFAULT_LOCALID, DEFAULT_LOCALNAME, REACTION_TIME, MOTION_TIME, RESPONSE_TIME, retrievedTime);
        score2 = new Score(DEFAULT_LOCALID, DEFAULT_LOCALNAME, REACTION_TIME, MOTION_TIME, RESPONSE_TIME, retrievedTime);
        score3 = new Score(DEFAULT_LOCALID, DEFAULT_LOCALNAME, 123, 456, 579, System.currentTimeMillis());
        badScore = new Score(DEFAULT_LOCALID, DEFAULT_LOCALNAME, 0, 0, 0, 0);
    }

    @Test
    public void testEquals() {
        assertEquals(score1, score2);
    }

    @Test
    public void testNotEquals() {
        assertNotEquals(score1, score3);
    }

    @Test
    public void testHashcodeEqual() {
        assertEquals(score1.hashCode(), score2.hashCode());
    }

    @Test
    public void testHashcodeNotEqual() {
        assertNotEquals(score1.hashCode(), score3.hashCode());
    }

    @Test
    public void testScoreFromGoodDataPacket() {
        Score score = Score.createScoreFromDataPacket(GOOD_DATA_PACKET);
        assertEquals(score1, score);
    }

    @Test
    public void testScoreFromBadDataPacket() {
        Score score = Score.createScoreFromDataPacket(BAD_DATA_PACKET);
        assertEquals(badScore, score);
    }
}
