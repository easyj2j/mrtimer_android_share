package work.johnmac.mrtimer.model;

import android.test.mock.MockContext;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for SqlDbHelper class.
 */
public class SqlDbHelperTest {
    @Before
    public void setup() {
        // Disable android.util.Log calls
        SqlDbHelper.DEBUG = false;
    }

    @Test
    public void testGetInstance() {
        SqlDbHelper dbHelper = SqlDbHelper.getInstance(new MockContext());
        assertNotNull(dbHelper);
    }

    @Test
    public void testSingleton() {
        SqlDbHelper dbHelper1 = SqlDbHelper.getInstance(new MockContext());
        SqlDbHelper dbHelper2 = SqlDbHelper.getInstance(new MockContext());
        assertEquals(dbHelper1, dbHelper2);
    }
}
