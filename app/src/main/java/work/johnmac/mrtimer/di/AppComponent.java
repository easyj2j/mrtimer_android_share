package work.johnmac.mrtimer.di;

import dagger.Component;
import work.johnmac.mrtimer.App;

@ApplicationScope
@Component(modules = {AppModule.class, RestModule.class, BluetoothModule.class})
public interface AppComponent {
    /**
     * @param app the class to be injected.
     */
    void inject(App app);
}
