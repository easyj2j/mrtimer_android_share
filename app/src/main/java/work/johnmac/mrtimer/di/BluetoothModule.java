package work.johnmac.mrtimer.di;

import android.util.Log;

import dagger.Module;
import dagger.Provides;
import work.johnmac.mrtimer.device.BluetoothValidator;
import work.johnmac.mrtimer.device.TimerDeviceService;
import work.johnmac.mrtimer.device.TimerDriver;

/**
 * Provides a TimerDeviceService binding.
 */
@Module
public class BluetoothModule {
    public BluetoothModule() {
        Log.d("BluetoothModule", "ctor");
    }

    @Provides
    @ApplicationScope
    BluetoothValidator provideBluetoothValidator() {
        Log.d("BluetoothModule", "BluetoothValidator");
        return new BluetoothValidator();
    }

    @Provides
    @ApplicationScope
    TimerDriver provideTimerDriver() {
        Log.d("BluetoothModule", "TimerDriver");
        return new TimerDriver();
    }

/*
    @Provides
    @ApplicationScope
    BluetoothConnection provideBluetoothConnection(TimerDriver timerDriver) {
        Log.d("BluetoothModule", "BluetoothConnection");
        return new BluetoothConnection(timerDriver);
    }
*/

    @Provides
    @ApplicationScope
    TimerDeviceService provideTimerDeviceService(TimerDriver timerDriver) {
        Log.d("BluetoothModule", "TimerDeviceService");
        return new TimerDeviceService(timerDriver);
    }
}
