package work.johnmac.mrtimer.di;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import work.johnmac.mrtimer.model.Score;
import work.johnmac.mrtimer.rest.ScoreJson;
import work.johnmac.mrtimer.rest.RestApi;
import work.johnmac.mrtimer.rest.RestClient;

/**
 * Provides Gson and RestApi bindings that don't change over the app lifetime.
 */
@Module
public class RestModule {
    private static final String APIKEY_HEADER = "ApiKey";
    private static final String APIKEY = "APIKEY";
    private static final String AUTH_HEADER = "Authorization";
    private static final String AUTHKEY = "Basic AUTHKEY";

    private final String hostUrl;

    public RestModule(String hostUrl) {
        this.hostUrl = hostUrl;
    }

    @Provides
    @ApplicationScope
    String provideHostUrl() {
        return hostUrl;
    }

    @Provides
    @ApplicationScope
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder()
        .registerTypeAdapter(Score.class, new ScoreJson());

        return gsonBuilder.create();
    }

    @Provides
    @ApplicationScope
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        // Level.BASIC, HEADERS, or BODY - see http://square.github.io/okhttp/3.x/logging-interceptor
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC); //BODY
        return httpLoggingInterceptor;
    }

    @Provides
    @ApplicationScope
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
        // Add headers with required creds to all requests
        .addNetworkInterceptor(chain -> {
            Request request = chain.request().newBuilder()
                    .addHeader(APIKEY_HEADER, APIKEY)
                    .addHeader(AUTH_HEADER, AUTHKEY).build();
            return chain.proceed(request);
        })
        //.addNetworkInterceptor(new StethoInterceptor())
        .addInterceptor(httpLoggingInterceptor);
        return okHttpBuilder.build();
    }

    @Provides
    @ApplicationScope
    Retrofit provideRetrofit(String hostUrl, Gson gson, OkHttpClient okHttpClient) {
        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(hostUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient);
        return retrofitBuilder.build();
    }

    @Provides
    @ApplicationScope
    RestClient provideRestClient(Gson gson, Retrofit retrofit) {
        return new RestClient(gson, retrofit.create(RestApi.class));
    }
}
