package work.johnmac.mrtimer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import com.appyvet.rangebar.RangeBar;

import java.util.Collection;

import work.johnmac.mrtimer.databinding.FragmentSettingsBinding;

/**
 * 4/19/2017
 * <p>
 * Timer options:
 * Trigger source: LED, speaker, both.
 * Trigger minimum and maximum random delay times.
 *
 * @author JMac
 */
public final class SettingsFragment extends Fragment {
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "SettingsFragment";

    interface SettingsChangedListener {
        void onConnect();
        void onTriggerChanged();
        void onDelayRangeChanged();
    }
    private SettingsChangedListener settingsChangedListener;

    // Trigger offset used to retrieve corresponding RadioButton position in RadioGroup.
    private static final int TRIGGER_OFFSET = 1;
    // Note that the corresponding TimerDriver constants are specified as byte (char).
    @VisibleForTesting
    public static final int TRIGGER_LED_PREF = 1;
    @VisibleForTesting
    public static final int TRIGGER_SPEAKER_PREF = 2;
    @VisibleForTesting
    public static final int TRIGGER_BOTH_PREF = 3;
    public static final int TRIGGER_DEFAULT = TRIGGER_LED_PREF;

    public static final String SELECTED_DEVICE_DEFAULT = "";

    private static final int DELAY_MAX_MAX = 8;
    public static final int MIN_DELAY_DEFAULT = 2;
    public static final int MAX_DELAY_DEFAULT = 7;

    // Preferences
    public static final String SELECTED_DEVICE  = "selectedDevice";  // string
    private static final String SELECTED_DEVICE_POSITION = "selectedDevicePosition"; // int
    private static final String IS_BT_CONNECTED = "isBtConnected";        // boolean
    public static final String CONNECTED_DEVICE = "connectedDevice"; // string
    public static final String UPLOAD = "upload";                    // boolean
    public static final String MIN_DELAY = "minDelay";               // int
    public static final String MAX_DELAY = "maxDelay";               // int
    public static final String TRIGGER = "trigger";                  // int
    public static final String AUTO_RECONNECT = "autoReconnect";     // boolean
    public static final String SHOW_DELAYS = "showDelays";           // boolean

    private SharedPreferences preferences;
    private FragmentSettingsBinding dataBindings;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setRetainInstance(true);

        try {
            settingsChangedListener = (SettingsChangedListener)context;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    context.toString() + " must implement SettingsChangedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        dataBindings = FragmentSettingsBinding.inflate(inflater);
        dataBindings.setEventHandlers(this);

        String aboutString = getResources().getString(R.string.about);
        dataBindings.timerAbout.setText(String.format(aboutString, BuildConfig.VERSION_NAME));

        setWidgetsFromPreferences();
        return dataBindings.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        dataBindings.deviceSpinner.setAdapter(new ArrayAdapter<>(
                getActivity(),
                R.layout.spinner_item,
                ((MainActivity)getActivity()).retrieveDeviceNames()));
    }

    @Override
    public void onStart() {
        super.onStart();
        setSelectedDeviceNameFromPreferences();
    }

    // R.id.device_spinner, R.id.group_spinner
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.equals(dataBindings.deviceSpinner)) {
            String selectedDeviceName = (String)parent.getItemAtPosition(position);
            preferences.edit().putString(SELECTED_DEVICE, selectedDeviceName)
                              .putInt(SELECTED_DEVICE_POSITION, position).apply();
        } else if (parent.equals(dataBindings.groupSpinner)) {
        }
    }

    /**
     * 0 <= index <= DELAY_MAX_MAX - 1
     * 1 <= minDelay <= maxDelay <= DELAY_MAX_MAX
     * => delay = index + 1
     * Called every time button (pin) slides over or lands on interval.
     * Not called on passed-over intervals when button moved by tap to interval.
     * Only one button can move at a time.
     * When right button is moved to the left of left button -> right button becomes left button.
     */
    // R.id.delay_interval
    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex,
                              String leftPinValue, String rightPinValue) {
        if(DEBUG) {Log.d(TAG, "onRangeChangeListener " + leftPinIndex + " : " + rightPinIndex); }
        preferences.edit()
                .putInt(MIN_DELAY, leftPinIndex + 1)
                .putInt(MAX_DELAY, rightPinIndex + 1).apply();
        settingsChangedListener.onDelayRangeChanged();
    }

    // R.id.connectButton
    public void onClick(View v) {
        settingsChangedListener.onConnect();
    }

    // R.id.auto_reconnect_checkbox, R.id.upload_checkbox
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView.getId() == R.id.upload_checkbox) {
            preferences.edit().putBoolean(UPLOAD, isChecked).apply();
        } else if (buttonView.getId() == R.id.auto_reconnect_checkbox) {
            preferences.edit().putBoolean(AUTO_RECONNECT, isChecked).apply();
        }
    }

    /**
     * Saves the R.id.trigger source selection.
     */
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int triggerSource = TRIGGER_DEFAULT;
        switch (checkedId) {
            case R.id.trigger_led:
                triggerSource = TRIGGER_LED_PREF;
                break;
            case R.id.trigger_speaker:
                triggerSource = TRIGGER_SPEAKER_PREF;
                break;
            case R.id.trigger_both:
                triggerSource = TRIGGER_BOTH_PREF;
                break;
            default:
                if(DEBUG) {
                    Log.e(TAG, "onCheckedChanged(), unknown resource id: " + checkedId);
                }
                break;
        }
        preferences.edit().putInt(TRIGGER, triggerSource).apply();
        settingsChangedListener.onTriggerChanged();
    }

    /**
     * Only called if Airplane Mode is on &/or Bluetooth is off.
     * In these cases, onActivityCreated & onStart are called before
     * TimerDeviceService.retrieveDeviceNames is populated.
     */
    public void setDeviceNames(Collection<String> timerNames) {
        // OK, type safety enforced at method invocation.
        @SuppressWarnings("unchecked")
        ArrayAdapter<String> deviceAdapter = (ArrayAdapter<String>)dataBindings.deviceSpinner.getAdapter();
        deviceAdapter.clear();
        deviceAdapter.addAll(timerNames);
        deviceAdapter.notifyDataSetChanged();
        setSelectedDeviceNameFromPreferences();
    }

    public void setConnectedDeviceText() {
        String connectedDevice = preferences.getString(CONNECTED_DEVICE, "");
        dataBindings.connectedDevice.setText(connectedDevice);
        boolean isConnected = !connectedDevice.isEmpty();
        preferences.edit().putBoolean(IS_BT_CONNECTED, isConnected).apply();
        enableTimerSettingsWidgets(isConnected);
    }

    private void enableTimerSettingsWidgets(boolean isConnected) {
        dataBindings.triggerLed.setEnabled(isConnected);
        dataBindings.triggerSpeaker.setEnabled(isConnected);
        dataBindings.triggerBoth.setEnabled(isConnected);
        dataBindings.delayInterval.setEnabled(isConnected);
    }

    // Set all widgets except spinners.
    private void setWidgetsFromPreferences() {
        boolean autoReconnect = preferences.getBoolean(AUTO_RECONNECT, false);
        dataBindings.autoReconnectCheckbox.setChecked(autoReconnect);

        boolean upload = preferences.getBoolean(UPLOAD, false);
        dataBindings.uploadCheckbox.setChecked(upload);

        // RadioButton position starts at zero.
        int checkedId = dataBindings.trigger.getChildAt(
                preferences.getInt(TRIGGER, TRIGGER_DEFAULT) - TRIGGER_OFFSET).getId();
        dataBindings.trigger.check(checkedId);

        String connectedDevice = preferences.getString(CONNECTED_DEVICE, "");
        dataBindings.connectedDevice.setText(connectedDevice);

        int minDelay = preferences.getInt(MIN_DELAY, MIN_DELAY_DEFAULT);
        int maxDelay = preferences.getInt(MAX_DELAY, MAX_DELAY_DEFAULT);
        dataBindings.delayInterval.setRangePinsByIndices(minDelay - 1, maxDelay - 1);

        boolean isConnected = preferences.getBoolean(IS_BT_CONNECTED, false);
        enableTimerSettingsWidgets(isConnected);
    }

    private void setSelectedDeviceNameFromPreferences() {
        int selectedDevicePosition = preferences.getInt(SELECTED_DEVICE_POSITION, 0);
        dataBindings.deviceSpinner.setSelection(selectedDevicePosition, true);
    }
}
