package work.johnmac.mrtimer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.LayoutRes;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import work.johnmac.mrtimer.model.Score;
import work.johnmac.mrtimer.device.BluetoothFragment;
import work.johnmac.mrtimer.device.BluetoothValidator;
import work.johnmac.mrtimer.device.TimerDeviceService;
import work.johnmac.mrtimer.rest.RestClient;
import work.johnmac.mrtimer.utility.TristateButton;

import static android.bluetooth.BluetoothAdapter.ACTION_STATE_CHANGED;
import static android.bluetooth.BluetoothAdapter.EXTRA_STATE;
import static android.bluetooth.BluetoothAdapter.STATE_OFF;
import static android.bluetooth.BluetoothAdapter.STATE_ON;
import static android.bluetooth.BluetoothAdapter.STATE_TURNING_OFF;

import static work.johnmac.mrtimer.device.BluetoothValidator.BT_AIRPLANE_MODE_OFF;
import static work.johnmac.mrtimer.device.BluetoothValidator.BT_REQUEST_ENABLE;
import static work.johnmac.mrtimer.ScoreFragment.SELECTED_USER;
import static work.johnmac.mrtimer.SettingsFragment.*;
import static work.johnmac.mrtimer.utility.TristateButton.*;

/**
 * Retrieves a score from an external timing device via Bluetooth.
 * Stores the score in a Sqlite DB and displays it.
 * Displays a list of retrieved scores.
 * Sends control commands to the timer.
 * Optionally, uploads the scores to a ReST service.
 * Provides different layouts for phones vs tablets.
 */
public class MainActivity extends AppCompatActivity
                          implements ScoreFragment.TristateListener,
                                     SettingsFragment.SettingsChangedListener,
                                     InsertScoreTask.NewScoreUpdater {

    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "MainActivity";

    // BluetoothAdapter.ACTION_STATE_CHANGED
    private final BroadcastReceiver bluetoothStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int btState = intent.getIntExtra(EXTRA_STATE, STATE_OFF);
            if (DEBUG) {
                Log.d(TAG, "bluetoothStateReceiver: Bluetooth state = " + btState);
            }
            if ((btState == STATE_TURNING_OFF)  && (deviceService != null)){
                deviceService.timerDisconnect();
            } else if ((btState == STATE_ON)) {
                setupBluetoothService();
            }
        }
    };

    private static final String USER_DEFAULT = "B Lee";

    // Set in MainPagerAdapter.
    @VisibleForTesting
    String scoreFragmentTag;
    @VisibleForTesting
    String scoresFragmentTag;
    @VisibleForTesting
    String settingsFragmentTag;

    private SharedPreferences preferences;
    private boolean bluetoothConnected;

    TimerDeviceService deviceService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId()); // R.layout.activity_main or activity_twopane

        preferences = getPreferences(Context.MODE_PRIVATE);

        deviceService = ((App)getApplication()).getTimerDeviceService();
        deviceService.setActivityOnTimerDriver(this);
        setupBluetoothService();

        ViewPager mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(new MainPagerAdapter(getSupportFragmentManager()));

        // Keep all fragments in memory (2 on each side of current fragment)
        mViewPager.setOffscreenPageLimit(2);

        // Check for two panes
        if(findViewById(R.id.score_container) != null) { // Only in values-w900dp-land
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.score_container, new ScoreFragment(), scoreFragmentTag)
                    .commit();
            // Initially display scoresFragment in 2nd pane
            mViewPager.setCurrentItem(1);
        }

        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(mViewPager);

        registerReceiver(bluetoothStateReceiver, new IntentFilter(ACTION_STATE_CHANGED));
    }

    /**
     * activity_main_alias is an alias that points to the layout file to be inflated by MainActivity.
     * There are multiple activity_main_alias's defined as alternative resources, with Android
     * picking the appropriate one based on the install device.
     *
     * @return layout.id of associated resource pointed to by activity_main_alias
     */
    @SuppressWarnings("SameReturnValue")
    @LayoutRes
    private int getLayoutResId() {
        return R.layout.activity_main_alias;
    }

    // Place deviceService in retained fragment to persist btConnection across config changes
    private void setupBluetoothService() {
        BluetoothValidator bluetoothValidator =  ((App)getApplication()).getBluetoothValidator();
        bluetoothValidator.setActivity(this);
        FragmentManager fm = getSupportFragmentManager();
        BluetoothFragment bluetoothFragment = (BluetoothFragment)fm.findFragmentByTag("BtFragment");
        if(bluetoothFragment == null) {
            bluetoothConnected = bluetoothValidator.btSetup();
            if(bluetoothConnected) {
                deviceService.btGetPairedTimers();
                setDeviceNames(retrieveDeviceNames());
                bluetoothFragment = new BluetoothFragment();
                fm.beginTransaction().add(bluetoothFragment, "BtFragment").commit();
                bluetoothFragment.setDeviceService(deviceService);
            }
        } else {
            deviceService = bluetoothFragment.getDeviceService();
            autoStart();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(bluetoothConnected) {
            autoStart();
        }
    }

    /*
     * isFinishing() == true: backNav/closeApp & Activity.finish
     * isFinishing() == false: config changes
     */
    @Override
    protected void onDestroy() {
        if(DEBUG) {
            Log.d(TAG, "onDestroy: unregister bluetoothStateReceiver");
        }
        unregisterReceiver(bluetoothStateReceiver);
        super.onDestroy();
    }

    private void autoStart() {
        if(preferences.getBoolean(SettingsFragment.AUTO_RECONNECT, false)) {
            onConnect();
        }
    }

    // Callbacks from BluetoothValidator.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (DEBUG) { Log.d(TAG, "onActivityResult(): " + requestCode); }

        switch (requestCode) {
            case BT_AIRPLANE_MODE_OFF:
                setupBluetoothService();
                break;
            case BT_REQUEST_ENABLE:
                if (RESULT_OK == resultCode) {
                    // Taken care of by bluetoothStateReceiver
                    //setupBluetoothService();
                } else {
                    onBtConnectionChanged(false);
                }
                break;
            default:
                if (DEBUG) {
                    Log.e(TAG, "onActivityResult(): no match for requestCode " + requestCode);
                }
        }
    }

    //******************* Fragment commands ************************************//

    private void setDeviceNames(List<String> timerNames) {
        SettingsFragment settingsFragment =
                (SettingsFragment)getSupportFragmentManager().findFragmentByTag(settingsFragmentTag);
        if(settingsFragment != null) {
            settingsFragment.setDeviceNames(timerNames);
        }
    }

    private void setConnectedDeviceText(String connectedDeviceName) {
        String deviceName = (connectedDeviceName == null) ? "" : connectedDeviceName;
        preferences.edit().putString(CONNECTED_DEVICE, deviceName).apply();

        SettingsFragment settingsFragment =
                (SettingsFragment)getSupportFragmentManager().findFragmentByTag(settingsFragmentTag);
        if(settingsFragment != null) {
            settingsFragment.setConnectedDeviceText();
        }
    }

    /**
     * Zeros the score in ScoreFragment.
     */
    public void zeroScore() {
        ScoreFragment scoreFragment =
                (ScoreFragment)getSupportFragmentManager().findFragmentByTag(scoreFragmentTag);
        if(scoreFragment != null) {
            scoreFragment.zeroScore();
        }
    }

    /**
     * Called by BluetoothConnection via MainHandler when a raw score is retrieved.
     *
     * Adds the user name and inserts the score in the db.
     * Forwards the new full score to ScoreFragment & ScoresFragment.
     * Optionally uploads the score.
     *
     * @param rawScore the new Score
     */
    public void finalizeScoreAndUpdate(Score rawScore) {
        if(rawScore.getRetrieved() == 0) {
            // Bad score: displays as zeros in ScoreFragment
            // but not displayed in ScoresFragment or inserted into Db.
            return;
        }
        String userName = preferences.getString(SELECTED_USER, USER_DEFAULT);
        new InsertScoreTask(this).execute(rawScore, userName, getContentResolver());
    }

    @Override
    public void updateScores(Score newScore) {
        updateScoreFragment(newScore);
        updateScoresFragment(newScore);
        uploadScore(newScore);
    }

    private void updateScoreFragment(Score newScore) {
        ScoreFragment scoreFragment =
                (ScoreFragment)getSupportFragmentManager().findFragmentByTag(scoreFragmentTag);
        if(scoreFragment != null) {
            scoreFragment.updateScore(newScore);
        }
    }

    private void updateScoresFragment(Score newScore) {
        ScoresFragment scoresFragment =
                (ScoresFragment)getSupportFragmentManager().findFragmentByTag(scoresFragmentTag);
        if(scoresFragment != null) {
            scoresFragment.updateScore(newScore);
        }
    }

    //********************** Upload ***************************************//

    @VisibleForTesting
    void uploadScore(Score newScore) {
        if(preferences.getBoolean(SettingsFragment.UPLOAD, false)) {
            if(isDataConnectionAvailable()) {
                RestClient restClient = ((App)getApplication()).getRestClient();
                restClient.insertScore(newScore);
                Toast.makeText(this, "Score uploaded", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "updateScore: score uploaded");
            } else {
                Toast.makeText(this, "Not connected", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "updateScore: not connected");
            }
        }
    }

    private boolean isDataConnectionAvailable() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //******************** Timer commands ***********************************//

    List<String> retrieveDeviceNames() {
        return (deviceService == null) ? Collections.emptyList() : deviceService.retrieveDeviceNames();
    }

    /**
     * Called by ScoreFragment tristateButton.onClick
     */
    @Override
    public void onTristateChanged() {
        if(deviceService == null) { return; }

        int tristate = preferences.getInt(TRISTATE, TRISTATE_TIMER_DISCONNECTED);
        switch (tristate) {
            case TRISTATE_TIMER_DISCONNECTED:
                // Try to connect
                onConnect();
                break;
            case TRISTATE_TIMER_LATCHED:
                // Was latched, now reset
                setTristateButton(TRISTATE_TIMER_RESET);
                deviceService.timerLatchOrReset(false);
                break;
            case TRISTATE_TIMER_RESET:
                // Was running, now latch timer display.
                setTristateButton(TRISTATE_TIMER_LATCHED);
                deviceService.timerLatchOrReset(true);
                break;
            default:
                Log.e(TAG, "onTristateChanged, unknown state: " + tristate);
        }
    }

    /**
     * Called from TimerDriver on BT connected or disconnected
     */
    public void onBtConnectionChanged(boolean isConnected) {
        if(isConnected) {
            // Stop screen blanking while connected.
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            setTristateButton(TristateButton.TRISTATE_TIMER_RESET);

            String connectedDevice = preferences.getString(CONNECTED_DEVICE, "");
            if(connectedDevice.isEmpty()) {
                Log.e(TAG, "onBtConnectionChanged isConnected but connectedDevice.isEmpty");
                return;
            }
            String toastString = String.format(getString(R.string.handler_bt_connected), connectedDevice);
            Toast.makeText(this, toastString, Toast.LENGTH_SHORT).show();

            deviceService.timerRequestOutput();
            updateTimerSettings();
            setConnectedDeviceText(connectedDevice);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            setTristateButton(TRISTATE_TIMER_DISCONNECTED);
            setConnectedDeviceText("");

            // toastString overlaps with connected toast
        }
    }

    private void updateTimerSettings() {
        onTriggerChanged();
        onDelayRangeChanged();
    }

    private void setTristateButton(int tristate) {
        preferences.edit().putInt(TRISTATE, tristate).apply();
        ScoreFragment scoreFragment =
                (ScoreFragment)getSupportFragmentManager().findFragmentByTag(scoreFragmentTag);
        if(scoreFragment != null) {
            scoreFragment.setTristate();
        }
    }

    /**
     * Called by SettingsFragment connectButton.onClick
     */
    @Override
    public void onConnect() {
        if(deviceService != null) {
            deviceService.timerConnect(preferences.getString(SELECTED_DEVICE, SELECTED_DEVICE_DEFAULT));
        }
    }

    /**
     * Sends min/max trigger delay commands to timer.
     */
    @Override
    public void onDelayRangeChanged() {
        if(deviceService != null) {
            deviceService.timerSetDelays(
                    preferences.getInt(MIN_DELAY, MIN_DELAY_DEFAULT),
                    preferences.getInt(MAX_DELAY, MAX_DELAY_DEFAULT)
            );
        }
    }

    @Override
    public void onTriggerChanged() {
        if(deviceService != null) {
            deviceService.timerSetTrigger(preferences.getInt(TRIGGER, TRIGGER_DEFAULT));
        }
    }

    //***********************************************************************//

    private class MainPagerAdapter extends FragmentPagerAdapter {
        private final String[] tabTitles =
                MainActivity.this.getResources().getStringArray(R.array.tab_titles);

        MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        // instantiateItem(position) calls getItem(position)
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            // Only creates fragment if not previously created (determined by existence of tag)
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            String tag = fragment.getTag();

            if(position == 0) { scoreFragmentTag = tag; } else
            if(position == 1) { scoresFragmentTag = tag; } else
            if(position == 2) { settingsFragmentTag = tag; }

            return fragment;
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) { return new ScoreFragment(); }
            if(position == 1) { return new ScoresFragment(); }
            if(position == 2) { return new SettingsFragment(); }
            return null;
        }

        @Override
        public int getCount() { return tabTitles.length; }

        @Override
        public CharSequence getPageTitle(int position) { return tabTitles[position]; }
    }
}
