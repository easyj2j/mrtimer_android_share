package work.johnmac.mrtimer;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.os.AsyncTask;

import work.johnmac.mrtimer.model.Score;

import static work.johnmac.mrtimer.model.ScoreProvider.SCORE_URI;
import static work.johnmac.mrtimer.model.SqlDbHelper.ID;
import static work.johnmac.mrtimer.model.SqlDbHelper.NAME;

/**
 * Inserts a score into Db via the ScoreProvider then
 * returns a new immutable Score containing a new Id and userName.
 */

class InsertScoreTask extends AsyncTask<Object, Void, Score> {
    interface NewScoreUpdater {
        void updateScores(Score newScore);
    }

    private final NewScoreUpdater notifier;

    InsertScoreTask(NewScoreUpdater notifier) {
        this.notifier = notifier;
    }

    @Override
    protected Score doInBackground(Object... params) {
        Score score = (Score)params[0];
        String userName = (String)params[1];
        ContentResolver resolver = (ContentResolver)params[2];

        ContentValues cv = score.toContentValues();
        cv.put(NAME, userName);
        long scoreId = ContentUris.parseId(resolver.insert(SCORE_URI, cv));
        cv.put(ID, scoreId);
        return new Score(cv);
    }

    @Override
    protected void onPostExecute(Score newScore) {
        notifier.updateScores(newScore);
    }
}
