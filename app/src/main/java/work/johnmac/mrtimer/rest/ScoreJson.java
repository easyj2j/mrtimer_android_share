package work.johnmac.mrtimer.rest;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import work.johnmac.mrtimer.model.Score;

import static work.johnmac.mrtimer.model.SqlDbHelper.MOTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.REACTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.RESPONSE;
import static work.johnmac.mrtimer.model.SqlDbHelper.RETRIEVED;
import static work.johnmac.mrtimer.utility.DateConverter.convertRawToReSTDate;
import static work.johnmac.mrtimer.utility.DateConverter.convertReSTDateToRaw;

/**
 * Used in RestClient by Gson as TypeAdapter.
 * The ScoreReSTServer at MainActivity.HOST_URL returns capital-case Json properties.
 */
public class ScoreJson implements JsonSerializer<Score>, JsonDeserializer<Score> {
    // These are different than the SqlDbHelper equivalents.
    private static final String LOCALID = "LocalID";
    private static final String LOCALNAME = "LocalName";

    @Override
    public JsonElement serialize(Score score, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(LOCALID, score.getID());
        jsonObject.addProperty(LOCALNAME, score.getName());
        // ScoreReSTServer accepts lower-case Json properties;
        // these are capitalized to pass ScoreJsonTest.roundTrip().
        jsonObject.addProperty(toCapitalCase(REACTION), score.getReaction());
        jsonObject.addProperty(toCapitalCase(MOTION), score.getMotion());
        jsonObject.addProperty(toCapitalCase(RESPONSE), score.getResponse());
        jsonObject.addProperty(toCapitalCase(RETRIEVED), convertRawToReSTDate(score.getRetrieved()));
        return jsonObject;
    }

    @Override
    public Score deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
        JsonObject jsonObject = json.getAsJsonObject();
        // jsonObject.get(String memberName) returns null if memberName isn't found.
        String retrievedTime = jsonObject.get(toCapitalCase(RETRIEVED)).getAsString();
        long retrieved = convertReSTDateToRaw(retrievedTime);

        Score score = new Score(
                jsonObject.get(LOCALID).getAsLong(),
                jsonObject.get(LOCALNAME).getAsString(),
                jsonObject.get(toCapitalCase(REACTION)).getAsInt(),
                jsonObject.get(toCapitalCase(MOTION)).getAsInt(),
                jsonObject.get(toCapitalCase(RESPONSE)).getAsInt(),
                retrieved);
        return score;
    }

    private String toCapitalCase(String str) {
        String firstChar = str.substring(0, 1).toUpperCase();
        String theRemaining = str.substring(1);
        return firstChar + theRemaining;
    }
}
