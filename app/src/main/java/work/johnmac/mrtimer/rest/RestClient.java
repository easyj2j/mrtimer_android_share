package work.johnmac.mrtimer.rest;

import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import work.johnmac.mrtimer.BuildConfig;
import work.johnmac.mrtimer.model.Score;

/**
 * Implements RestApi client using Retrofit.
 */

public final class RestClient {
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "RestClient";

    // Retrofit implements RestApi
    private final RestApi retrofitSyncService;
    private final Gson gson;

    @VisibleForTesting
    int newScoreId = -1;
    @VisibleForTesting
    Score retrievedScore;
    @VisibleForTesting
    public List<Score> scores;

    public RestClient(Gson gson, RestApi retrofitSyncService) {
        if(DEBUG) { Log.d(TAG, "RestClient.ctor"); }
        this.gson = gson;
        this.retrofitSyncService = retrofitSyncService;
    }

    public void insertScore(Score score) {
        newScoreId = -1;
        Call<ResponseBody> call = retrofitSyncService.insertScore(score);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(DEBUG) {
                    setRequestText(call);
                    Log.d(TAG, "insertScore: Http " + response.code() + '\t' + response.message());
                }

                if (wasSuccessful(response)) {
                    List<String> values = response.headers().values("Location");
                    if (Collections.<String>emptyList() != values) {
                        String location = values.get(0);
                        String idString = location.substring(location.lastIndexOf('/') + 1);
                        newScoreId = Integer.parseInt(idString);

                        if (DEBUG) { Log.d(TAG, "insertScore, location: " + values.get(0)); }
                    } else {
                        if (DEBUG) { Log.e(TAG, "insertScore, no location header"); }
                    }
                } else {
                    if(DEBUG) { Log.e(TAG, "insertScore error, HTTP: " + response.code()); }
                }
            }

            // A network or processing exception; not an Http failure code.
            // One possibility: java.net.SocketTimeoutException: timeout
            // One possibility: java.net.ConnectException: Failed to connect to host
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(DEBUG) { Log.e(TAG, "insertScore, onFailure: " + t.getMessage()); }
            }
        });
    }

    // Never used or tested
    public void retrieveAllScores() {
        scores = null;
        Call<List<Score>> call = retrofitSyncService.retrieveAllScores();
        call.enqueue(new Callback<List<Score>>() {
            @Override
            public void onResponse(Call<List<Score>> call, Response<List<Score>> response) {
                if(DEBUG) {
                    setRequestText(call);
                    Log.d(TAG, "retrieveAllScores: Http " + response.code() + '\t' + response.message());
                }

                if (wasSuccessful(response)) {
                    try {
                        scores = response.body();
                        StringBuilder stringBuilder = new StringBuilder();
                        for (Score r : scores) {
                            stringBuilder.append(r.toString()).append('\n');
                        }
                        Log.d(TAG, "retrieveAllScores: " + stringBuilder.toString());
                    } catch (JsonSyntaxException e) {
                        Log.e(TAG, "retrieveAllScores: " + e);
                    }
                } else {
                    if(DEBUG) { Log.e(TAG, "retrieveAllScores error, HTTP: " + response.code()); }
                }
            }

            @Override
            public void onFailure(Call<List<Score>> call, Throwable t) {
                if(DEBUG) { Log.e(TAG, "retrieveAllScores, onFailure: " + t.getMessage()); }
            }
        });
    }

    // Can see Json string in OkHttpClient's HttpLoggingInterceptor output but
    // unable to figure out how to get it programmatically here.
    // Haven't tried in multiple versions later.
    public void retrieveScore(int id) {
        retrievedScore = null;
        Call<Score> call = retrofitSyncService.retrieveScore(id);
        call.enqueue(new Callback<Score>() {
            @Override
            public void onResponse(Call<Score> call, Response<Score> response) {
                if(DEBUG) {
                    setRequestText(call);
                    Log.d(TAG, "retrieveScore: Http " + response.code() + '\t' + response.message());
                }

                if (wasSuccessful(response)) {
                    try {
                        retrievedScore = response.body();
                        if(DEBUG) { Log.d(TAG, "retrieveScore: " + retrievedScore.toString()); }
                    } catch (JsonSyntaxException e) {   // | IOException e) {
                        if(DEBUG) { Log.e(TAG, "retrieveScore: " + e); }
                    }
                } else {
                    if(DEBUG) { Log.e(TAG, "retrieveScore error, HTTP: " + response.code()); }
                }
            }

            @Override
            public void onFailure(Call<Score> call, Throwable t) {
                if(DEBUG) { Log.e(TAG, "retrieveScore, onFailure: " + t.getMessage()); }
            }
        });
    }

    /**
     * Gets a raw JSON string and converts it to a Score.
     *
     * @param id the Score to retrieve
     */
    void retrieveScoreGson(int id) {
        retrievedScore = null;
        Call<ResponseBody> call = retrofitSyncService.retrieveScoreGson(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(DEBUG) {
                    setRequestText(call);
                    Log.d(TAG, "retrieveScoreGson: Http " + response.code() + '\t' + response.message());
                }

                if (wasSuccessful(response)) {
                    try {
                        String responseString = response.toString() + '\n' + response.body().toString();
                        String responseBodyString = response.body().string(); // json score
                        if(DEBUG) { Log.d(TAG, "retrieveScoreGson: " + responseString + '\n' + responseBodyString); }

                        retrievedScore = gson.fromJson(responseBodyString, Score.class);
                        if(DEBUG) { Log.d(TAG, "retrieveScoreGson: " + retrievedScore); }

                    } catch (JsonSyntaxException | IOException e) {
                        Log.e(TAG, "retrieveScoreGson: ", e);
                    }
                } else {
                    if(DEBUG) { Log.e(TAG, "retrieveScoreGson error, HTTP: " + response.code()); }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(DEBUG) { Log.e(TAG, "retrieveScoreGson, onFailure: " + t.getMessage()); }
            }
        });
    }

    // Never used or tested
    public void updateScore(int id, Score score) {
        Call<ResponseBody> call = retrofitSyncService.updateScore(id, score);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(DEBUG) {
                    setRequestText(call);
                    Log.d(TAG, "updateScore: Http " + response.code() + '\t' + response.message());
                }

                if (wasSuccessful(response)) {
                    try {
                        String responseString = response.toString() + '\n' + response.body().toString();
                        String responseBodyString = response.body().string();
                        if(DEBUG) { Log.d(TAG, "updateScore: " + responseString + '\n' + responseBodyString); }

                        // ...
                    } catch (JsonSyntaxException | IOException e) {
                        Log.e(TAG, "updateScore: ", e);
                    }
                } else {
                    if(DEBUG) { Log.e(TAG, "updateScore error, HTTP: " + response.code()); }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(DEBUG) { Log.e(TAG, "updateScore, onFailure: " + t.getMessage()); }
            }
        });
    }

    void deleteScore(int id) {
        Call<ResponseBody> call = retrofitSyncService.deleteScore(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(DEBUG) {
                    setRequestText(call);
                    Log.d(TAG, "deleteScore: Http " + response.code() + '\t' + response.message());
                }

                if (wasSuccessful(response)) {
                    retrievedScore = null;
                } else {
                    if(DEBUG) { Log.e(TAG, "deleteScore error, HTTP: " + response.code()); }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if(DEBUG) { Log.e(TAG, "deleteScore, onFailure: " + t.getMessage()); }
            }
        });
    }

    private void setRequestText(Call call) {
        String requestString = call.request().toString();
        RequestBody requestBody = call.request().body();
        String requestBodyString = (requestBody != null) ? requestBody.toString() : "no body";

        if(DEBUG) {
            Log.d(TAG, "setRequestText: requestString = " +
                    requestString + "\nrequestBody = " + requestBodyString);
        }
    }

    private boolean wasSuccessful(Response response) {
        boolean success = response.isSuccessful();
        if(DEBUG) {
            Log.d(TAG, "wasSuccessful: " + success + "\tHttp " +
                response.code() + '\t' + response.message());
        }
        return success;
    }
}
