package work.johnmac.mrtimer.rest;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

import work.johnmac.mrtimer.model.Score;

/**
 * Interface for ReST client.
 */

public interface RestApi {

    @POST("api/Score")
    Call<ResponseBody> insertScore(@Body Score score);

    // Get all Scores
    @GET("api/Score")
    Call<List<Score>> retrieveAllScores();

    // Get all IDs
    @GET("api/Score/{ids}")
    Call<List<Integer>> retrieveScoreIDs(@Path("ids") String ids);

    @GET("api/Score/{id}")
    Call<Score> retrieveScore(@Path("id") long id);

    @GET("api/Score/{id}")
    Call<ResponseBody> retrieveScoreGson(@Path("id") long id);

    @PUT("api/Score/{id}")
    Call<ResponseBody> updateScore(@Path("id") long id, @Body Score score);

    @DELETE("api/Score/{id}")
    Call<ResponseBody> deleteScore(@Path("id") long id);
}
