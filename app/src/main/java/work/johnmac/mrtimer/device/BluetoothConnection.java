package work.johnmac.mrtimer.device;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;

import work.johnmac.mrtimer.BuildConfig;

/**
 * Sets up and manages a single Bluetooth connection with a remote timer device.
 * Control commands are sent over the connection and scores from the timer are retrieved.
 *
 * Dependencies:
 *   TimerDriver, to pass new scores & connection status.
 *   BluetoothDevice, the BT paired timer to communicate with.
 */
public class BluetoothConnection implements TimerDriver.IBluetoothConnection {
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "BluetoothConnection";

    // Connection state constants.
    static final int BT_STATE_DISCONNECTED = 0; // IOThread stopped & null.
    static final int BT_STATE_CONNECTED    = 2; // IOThread connected to remoteBtDevice and in/outStreams retrieved.
    static final int BT_STATE_UNABLE_TO_CONNECT = 4; //

    static final int BT_CONNECTED_DEVICE_NAME    = 120;
    static final int BT_NOT_APPLICABLE           = 130;
    static final int BT_REASON_UNAVAILABLE       = 140;
    static final int BT_PAIRED_DEVICE_UNAVAIL    = 150;
    static final int BT_CONNECTION_DEVICE_TURNED_OFF = 160;
    static final int BT_CONNECTION_RESET_BY_PEER = 170;

    // Holds one of the connection state constants.
    private int connectionState = BT_STATE_DISCONNECTED;

    /**
     * setBtConnectionState, zeroScore, setRawScore
     */
    private final ITimerResults timerDriver;

    // BluetoothDevice just wraps a BT hardware address
    // Set in connect() as it can change.
    private BluetoothDevice btDevice;

    // BT device name. Just used for logging and in message to MainHandler.
    private String btDeviceName;    // "MRTimer#"

    // Handles all communications with btDeviceName.
    private IOThread ioThread;

    BluetoothConnection(ITimerResults timerDriver) {
        this.timerDriver = timerDriver;
    }

    private void setConnectionState(int state, int reason) {
        if (DEBUG) {
            Log.d(TAG, "setConnectionState(): " + connectionState + " -> " + state);
        }
        connectionState = state;
        timerDriver.setBtConnectionState(state, reason);
    }

    // Attempt to connect to remote device.
    public void connect(BluetoothDevice btDevice) {
        if (DEBUG) { Log.d(TAG, "connect()"); }

        // Close previous connection.
        disconnect();

        this.btDevice = btDevice;
        btDeviceName = btDevice.getName();

        if (BT_STATE_CONNECTED == connectionState) {
            // Already connected.
            Log.e(TAG, "connect: already connected");
            return;
        } else {
            // Start fresh.
            setConnectionState(BT_STATE_DISCONNECTED, BT_NOT_APPLICABLE);
        }

        try {
            ioThread = new IOThread();
        } catch (IOException e) {
            if (DEBUG) { Log.e(TAG, "connect():  caught IOException", e); }

            return;
        }

        // Connects localBtSocket to remoteBtDevice.
        ioThread.start(); // Calls IOThread.run which creates the new thread.
    }

    public void disconnect() {
        if (DEBUG) { Log.d(TAG, "disconnect()"); }

        if (ioThread != null) {
            try {
                // Closing inStream immediately breaks out of ioThread.read().
                if (ioThread.inStream != null) { ioThread.inStream.close(); }
                if (ioThread.outStream != null) { ioThread.outStream.close(); }
            } catch (IOException e) {
                if (DEBUG) { Log.e(TAG, "IOThread.disconnect():  caught IOException", e); }
            }

            ioThread = null;
        } else {
            setConnectionState(BT_STATE_DISCONNECTED, BT_NOT_APPLICABLE);
        }
    }

    public boolean write(byte[] outputBuffer) {
        return (ioThread == null) ? false : ioThread.write(outputBuffer);
    }

// ============================================================================

    /**
     * Called by BluetoothConnection.connect() to create the local Bluetooth socket
     * and attempt an outgoing connection to a listening remote device.
     * Uses a hidden method, <code>createRfcommSocket</code>, 
     * because UUID discovery is screwy on HTC Incredible2 & others.
     */
    private class IOThread extends Thread {
        // RFCOMM_CHANNEL = 1 for RFCOMM protocol (bluetooth spec) -
        //    this doesn't appear precisely correct
        private static final int RFCOMM_CHANNEL = 1;
        // Number of bytes sent by MRTimer (one response timing: !&motn&resp\n).
        private static final int TIMER_DATABYTE_COUNT = 12;
        private static final char ZERO_APP_DISPLAY = '!';

        private final byte[] inputBuffer;

        private BluetoothSocket localBtSocket;
        private InputStream inStream;
        private OutputStream outStream;

        @SuppressLint("NewApi")
        IOThread() throws IOException {
            if (DEBUG) { Log.d(TAG, "IOThread.ctor()"); }

            setName("MRTimerReader");

            inputBuffer = new byte[TIMER_DATABYTE_COUNT];

            // TODO: use create[Insecure]RfcommSocketToServiceRecord (by api)
            // and then in exception resort to createRfcommSocket.
            Method m;
            try {
                // Create the local Bluetooth socket.
                m = btDevice.getClass().getMethod("createRfcommSocket", int.class);
                localBtSocket = (BluetoothSocket) m.invoke(btDevice, RFCOMM_CHANNEL);

                if (DEBUG) { Log.d(TAG, "IOThread.ctor(): local BluetoothSocket created"); }
            }
            /*
             * createRfcommSocket() throws IOException, but since hidden, 
             *   catching it directly is not allowed -> catch Exception instead.
             * <p>
             * Method.invoke requires catching the following exceptions: 
             *  SecurityException | NoSuchMethodException | IllegalArgumentException |
             *  IllegalAccessException | InvocationTargetException
             *  <p>
             */
            catch (Exception e) {
                if (DEBUG) {
                    Log.e(TAG, "IOThread.ctor(): Unable to create local BluetoothSocket");
                }
                setConnectionState(BT_STATE_DISCONNECTED, BT_REASON_UNAVAILABLE);
                throw new IOException("Unable to create local BluetoothSocket \n" + e);
            }
        }

        // Connect to the remote device; get the I/O streams; notify Activity; call read().
        @Override
        public void run() {
            if (DEBUG) {
                Log.d(TAG, "IOThread.run(): connecting to " + btDevice + " (" + btDeviceName + ")");
            }

            // Connect to the remote device.
            try {
                // This is a blocking call and only returns on a successful
                // connection or an exception.
                localBtSocket.connect();
                setConnectionState(BT_STATE_CONNECTED, BT_NOT_APPLICABLE);
                if (DEBUG) {
                    Log.d(TAG, "IOThread.run(): connected to " + btDeviceName + " (" + btDevice + ")");
                }
            } catch (IOException e) {
                if (DEBUG) {
                    Log.e(TAG, "IOThread.run(): localBtSocket.connect() failed: caught exception ", e);
                }
                int reason = BT_REASON_UNAVAILABLE;
                // "read failed, socket might closed or timeout"
                if (e.getMessage().contains("socket might closed")) { // valid 7/28/17
                    reason = BT_CONNECTION_DEVICE_TURNED_OFF; // or timeout
                } else if (e.getMessage().contains("refused")) {      // not validated recently
                    reason = BT_PAIRED_DEVICE_UNAVAIL;
                }
                // possibly other reasons

                setConnectionState(BT_STATE_UNABLE_TO_CONNECT, reason);
                return;
            }

            try {
                inStream = localBtSocket.getInputStream();
                outStream = localBtSocket.getOutputStream();
                if (DEBUG) { Log.d(TAG, "IOThread.run(): inStream & outStream retrieved"); }
                setConnectionState(BT_STATE_CONNECTED, BT_CONNECTED_DEVICE_NAME);

            } catch (IOException e) {
                if (DEBUG) {
                    String stream = (inStream == null) ? "InputStream" : "OutputStream";
                    Log.e(TAG, "IOThread.run(): get" + stream + " failed: caught exception", e);
                }
                setConnectionState(BT_STATE_DISCONNECTED, BT_REASON_UNAVAILABLE);
            }

            read();
        }

        /**
         * Continuously read from the InputStream.
         *
         * If device disconnects (ie. physically turned off) there's an X sec
         *   timeout before exception is thrown in read().
         * Timeout is device and/or OS ver dependent. HTC Eris w 2.3: 20 sec, Nexus5X w 7.0: 5 sec.
         * Nothing tells app that the connection is gone.
         * Is there a setting for changing timeout?
         *
         * Reconnect() not currently enabled.
         * If device comes back online before timeout, app will auto-reconnect after the timeout.
         * If device comes back online after timeout, must manually reconnect; this is ok.
         * If try to manually reconnect before timeout, can't because original connection
         *   still shows as valid.
         */
        private void read() {
            if (DEBUG) { Log.d(TAG, "IOThread.read()"); }

            int bytesReadFromStream = 0;

            // Thread.interrupt won't stop the thread until some data arrives causing a read.
            while (!Thread.interrupted()) {
                try {
                    // Blocks until data is available or an exception is thrown.
                    // Closing inStream immediately breaks out of read().
                    // read(buffer, offset to write to, max bytes to read)
                    bytesReadFromStream += inStream.read(
                            inputBuffer, bytesReadFromStream, TIMER_DATABYTE_COUNT - bytesReadFromStream);

                    // A timer data packet starts with ZERO_APP_DISPLAY.
                    if (ZERO_APP_DISPLAY == inputBuffer[0]) {
                        // Start of a new data packet. Notify Activity to zero score.
                        timerDriver.zeroScore();
                    } else {
                        bytesReadFromStream = 0;
                    }

                    // TIMER_DATABYTE_COUNT is the data packet size transmitted by btDeviceName.
                    if (TIMER_DATABYTE_COUNT == bytesReadFromStream) {
                        bytesReadFromStream = 0;
                        // We have a complete data packet
                        timerDriver.setRawScore(inputBuffer);
                    }
                } catch (IOException e) {
                    if (DEBUG) {
                        Log.e(TAG, "IOThread.read(): caught exception", e);
                    }
                    int reason = BT_REASON_UNAVAILABLE;
                    /*
                     e.getMessage().contains:
                       bt socket closed:
                         one way to invoke this is to physically turn off the timer and wait
                         for the timeout exception.
                       reset by peer:
                         one reason is a mismatched baud rate between device &
                         BT module but there are others.
                    */
                    if (e.getMessage().contains("bt socket closed")) { // valid 10/10/17
                        reason = BT_CONNECTION_DEVICE_TURNED_OFF;
                    } else if (e.getMessage().contains("reset by peer")) {
                        reason = BT_CONNECTION_RESET_BY_PEER;
                    }
                    setConnectionState(BT_STATE_DISCONNECTED, reason);
                    break;
                }
            }
        }

        /**
         * Write outputBuffer to the device then clear buffer.
         *
         * @param outputBuffer contains timer commands
         */
        private boolean write(byte[] outputBuffer) {
            boolean success = false;
            if (outStream != null) {
                if (DEBUG) { Log.d(TAG, "IOThread.write(): " + new String(outputBuffer)); }

                try {
                    outStream.write(outputBuffer);

                    // Clear buffer ('\n' ignored by timer).
                    for (int i = 0; i < outputBuffer.length; i++) {
                        outputBuffer[i] = 0x0A; // '\n'
                    }
                    success = true;
                } catch (IOException e) {
                    if (DEBUG) {
                        Log.e(TAG, "IOThread.write(): write() failed: caught exception", e);
                    }
                    disconnect();
                    //reconnect();
                }
            } else {
                if (DEBUG) { Log.e(TAG, "IOThread.write(): outStream is null"); }
                disconnect();
                //reconnect();
            }
            return success;
        }
    }
}
