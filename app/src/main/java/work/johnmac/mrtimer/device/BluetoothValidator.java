package work.johnmac.mrtimer.device;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import work.johnmac.mrtimer.BuildConfig;
import work.johnmac.mrtimer.R;
import work.johnmac.mrtimer.utility.OkDialogFragment;

/**
 * Checks for Bluetooth support & that Bluetooth is enabled.
 *
 * Attempts to enable Bluetooth (BT) if off.
 *
 * If Airplane Mode (AM) is on, asks user to disable AM and enable BT.
 * [API 17 removed ability to programmatically disable airplane mode.]
 * Registers a broadcast receiver for notification that the user disabled AM.
 *
 * Dialogs possibly presented to user:
 *   Bluetooth not supported.
 *   Airplane mode on; turn off.
 *
 * Dependencies:
 *   Activity, for dialogs
 */

public final class BluetoothValidator {
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "BluetoothValidator";

    // onActivityResult codes.
    public static final int BT_AIRPLANE_MODE_OFF = 101;
    public static final int BT_REQUEST_ENABLE = 201;

    private BluetoothAdapter btAdapter;
    private Activity activity;

    public BluetoothValidator() {
        if(DEBUG) { Log.d(TAG, "BluetoothValidator.ctor"); }
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public boolean btSetup() {
        // Check for Bluetooth support.
        if (!isBluetoothSupported()) { return false; }

        // BT is supported; check that AirplaneMode is off (overrides BT).
        if (isAirplaneModeOn()) {
            // Ask user to turn off AM; see activity.onActivityResult() for callback.
            new AlertDialog.Builder(activity)
                    .setTitle(activity.getResources().getString(R.string.app_name))
                    .setMessage(activity.getResources().getString(R.string.dialog_bt_airplane_mode))
                    .setPositiveButton("OK", (d, w) ->
                            activity.startActivityForResult(
                                    new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS),
                                    BT_AIRPLANE_MODE_OFF))
                    .setNegativeButton("Cancel", (d, w) -> {})
                    .show();
            return false;
        }

        // AirplaneMode is off; check that BT is enabled.
        if (!btAdapter.isEnabled()) {
            btEnable();
            return false;
        }

        return true;
    }

    private boolean isBluetoothSupported() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            btAdapter = BluetoothAdapter.getDefaultAdapter();
        } else {
            BluetoothManager btManager =
                    (BluetoothManager)activity.getSystemService(Context.BLUETOOTH_SERVICE);
            btAdapter = btManager.getAdapter();
        }

        if (null == btAdapter) {
            // Let user know Bluetooth isn't supported.
            new OkDialogFragment()
                    .setMessage(activity.getResources().getString(R.string.dialog_bt_not_supported))
                    .show(activity.getFragmentManager(), "");
            return false;
        }
        return true;
    }

    /**
     * @return true if Airplane Mode is enabled
     */
    private boolean isAirplaneModeOn() {
        return Settings.Global.getInt(activity.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    private void btEnable() {
        // Ask user for permission to turn on BT; see activity.onActivityResult() for callback.
        activity.startActivityForResult(new Intent(
                BluetoothAdapter.ACTION_REQUEST_ENABLE), BT_REQUEST_ENABLE);
    }
}
