package work.johnmac.mrtimer.device;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.support.annotation.Nullable;
import android.support.v4.util.SimpleArrayMap;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import work.johnmac.mrtimer.BuildConfig;
import work.johnmac.mrtimer.MainActivity;

import static work.johnmac.mrtimer.SettingsFragment.SELECTED_DEVICE_DEFAULT;

/**
 * Not a real service; a layer between MainActivity & TimerDriver.
 *
 * Retrieves paired devices whose name starts with "MRTimer".
 * Forwards dis/connection requests and commands to TimerDriver.
 *
 * Dependencies:
 *   BluetoothAdapter, to retrieve paired devices
 *   TimerDriver, to connect & send commands to timer
 */

public final class TimerDeviceService {
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "TimerDeviceService";

    private static final String BT_DEVICE_NAME_FILTER = "MRTimer";

    private String prevSelectedDeviceName;
    private final SimpleArrayMap<String, BluetoothDevice> btDevices;

    private final BluetoothAdapter btAdapter;
    private final TimerDriver timerDriver;

    public TimerDeviceService(TimerDriver timerDriver) {
        if(DEBUG) { Log.d(TAG, "TimerDeviceService.ctor"); }
        this.timerDriver = timerDriver;
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        btDevices = new SimpleArrayMap<>(10);
    }

    public void setActivityOnTimerDriver(MainActivity activity) {
        timerDriver.setMainActivity(activity);
    }

    /**
     * Builds up the btDevices ArrayMap with paired Bluetooth devices
     * whose names start with BT_DEVICE_NAME_FILTER.
     *
     * @return false if no matching devices found.
     */
    public boolean btGetPairedTimers() {
        final Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();

        if ((pairedDevices == null) || pairedDevices.isEmpty()) {
            if (DEBUG) { Log.d(TAG, "btGetPairedDevice: no paired devices"); }
            return false;
        }

        for (BluetoothDevice device : pairedDevices) {
            String deviceName = device.getName();
            if (deviceName.startsWith(BT_DEVICE_NAME_FILTER)) {
                btDevices.put(deviceName, device);
                if (DEBUG) {
                    Log.d(TAG, "btGetPairedDevice(): " +
                            deviceName + " (" + device.getAddress() + ')');
                }
            }
        }
        return !btDevices.isEmpty();
    }

    /**
     * Populates SettingsFragment.deviceSpinner.
     *
     * @return A list of device names corresponding to the devices in pairedDevices.
     */
    public List<String> retrieveDeviceNames() {
        List<String> deviceNames = new ArrayList<>();
        for (int i = 0; i < btDevices.size(); i++) {
            deviceNames.add(btDevices.keyAt(i));
        }
        return deviceNames;
    }

    /**
     * Iterates through <code>btDevices</code> looking for <code>selectedDeviceName</code>.
     * Note: A paired device does not have to exist; must try connecting to find out.
     *
     * @return true if <code>selectedDeviceName</code> found
     */
    private @Nullable BluetoothDevice btGetSelectedDevice(String selectedDeviceName) {
        BluetoothDevice selectedDevice = null;
        if (btDevices.isEmpty()) {
            if (DEBUG) { Log.d(TAG, "btGetSelectedDevice: no paired timer devices"); }
        } else {
            for (int i = 0; i < btDevices.size(); i++) {
                if (btDevices.keyAt(i).equals(selectedDeviceName)) {
                    selectedDevice = btDevices.valueAt(i);
                    if (DEBUG) {
                        Log.d(TAG, "btGetSelectedDevice(): " +
                                selectedDeviceName + "( " + selectedDevice.getAddress() + ')');
                    }
                }
            }
        }
        return selectedDevice;
    }

    /**
     * Tries to connect to <code>selectedDeviceName</code>.
     *
     * @return false on failure
     */
    public boolean timerConnect(String selectedDeviceName) {
        if ((BluetoothConnection.BT_STATE_CONNECTED == timerDriver.getBtConnectionState()) &&
            (selectedDeviceName.equals(prevSelectedDeviceName))) {
            // Already connected to this device.
            return true;
        } else {
            prevSelectedDeviceName = selectedDeviceName;
            timerDriver.closeBtConnection();
            BluetoothDevice selectedDevice = btGetSelectedDevice(selectedDeviceName);
            if(selectedDevice != null) {
                // While doing discovery, connection is slower and more likely to timeout.
                btAdapter.cancelDiscovery();
                //Toast.makeText(activity, "Connecting to " + selectedDeviceName, Toast.LENGTH_SHORT).show();
                timerDriver.connectToTimer(selectedDevice);
                return true;
            }
        }
        return false;
    }

    /**
     * Closes Bluetooth connection to <code>btDevice</code>.
     */
    public void timerDisconnect() {
        timerDriver.closeBtConnection();
        prevSelectedDeviceName = SELECTED_DEVICE_DEFAULT;
    }


    /**
     * Sends "transmit score over Bluetooth" command to timer.
     * Requires a Bluetooth connection.
     */
    public void timerRequestOutput() {
        timerDriver.requestOutput();
    }

    public void timerLatchOrReset(boolean shouldLatch) {
        timerDriver.latchOrResetDisplay(shouldLatch);
    }

    public void timerSetDelays(int minDelay, int maxDelay) {
        timerDriver.setTriggerDelayLimits(minDelay, maxDelay);
    }

    public void timerSetTrigger(int triggerSource) {
        timerDriver.setTrigger(triggerSource);
    }
}
