package work.johnmac.mrtimer.device;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * A headless fragment to maintain the TimerDeviceService
 * (TimerDriver & BluetoothConnection) over config changes.
 */

public class BluetoothFragment extends Fragment {
    private TimerDeviceService deviceService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    // Not called on config changes with setRetainInstance(true)
    @Override
    public void onDestroy() {
        timerDisconnect();
        super.onDestroy();
    }

    public TimerDeviceService getDeviceService() {
        return deviceService;
    }

    public void setDeviceService(TimerDeviceService deviceService) {
        this.deviceService = deviceService;
    }

    private void timerDisconnect() {
        if(deviceService != null) {
            deviceService.timerDisconnect();
        }
    }
}
