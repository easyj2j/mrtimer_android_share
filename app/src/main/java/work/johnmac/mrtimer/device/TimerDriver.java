package work.johnmac.mrtimer.device;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.annotation.VisibleForTesting;
import android.util.Log;
import android.widget.Toast;

import work.johnmac.mrtimer.BuildConfig;
import work.johnmac.mrtimer.MainActivity;
import work.johnmac.mrtimer.R;
import work.johnmac.mrtimer.model.Score;
import work.johnmac.mrtimer.utility.TristateButton;

import static work.johnmac.mrtimer.SettingsFragment.CONNECTED_DEVICE;
import static work.johnmac.mrtimer.device.BluetoothConnection.BT_CONNECTION_DEVICE_TURNED_OFF;
import static work.johnmac.mrtimer.device.BluetoothConnection.BT_PAIRED_DEVICE_UNAVAIL;
import static work.johnmac.mrtimer.utility.TristateButton.TRISTATE_TIMER_DISCONNECTED;

/**
 * Connects to and disconnects from a Bluetooth paired timer.
 * Receives notification of Bluetooth connection state changes.
 * Sends commands to the timer.
 * Receives new scores from the timer.
 */
public class TimerDriver implements ITimerResults {
    static boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "TimerDriver";

    private static final String NOT_CONNECTED = "isBtConnectionReady(): not connected";

    // Offset to convert int to ascii char.
    @VisibleForTesting
    static final byte INT_TO_CHAR   = 0x30;

    // Standalone commands.
    @VisibleForTesting
    static final byte DO_NOTHING             = 0x0A; // '\n' - useful to clear buffer between commands
    private static final byte ENABLE_OUTPUT  = 0x6F; // 'o'
    private static final byte DISABLE_OUTPUT = 0x6E; // 'n'
    private static final byte LATCH_DISPLAY  = 0x64; // 'd'
    private static final byte ENABLE_DISPLAY = 0x65; // 'e'
    private static final byte FIRMWARE_VERSION = 0x66; // 'f'

    // The following are preludes and require a subsequent control byte.
    private static final byte DELAYTIME_MIN   = 0x4E;      // 'N'
    private static final byte DELAYTIME_MAX   = 0x58;      // 'X'
        private static final byte DELAY_ONE   = 0x31;        // '1'
        private static final byte DELAY_TWO   = 0x32;        // '2'
        private static final byte DELAY_THREE = 0x33;        // '3'
        private static final byte DELAY_FOUR  = 0x34;        // '4'
        private static final byte DELAY_FIVE  = 0x35;        // '5'
        private static final byte DELAY_SIX   = 0x36;        // '6'
        private static final byte DELAY_SEVEN = 0x37;        // '7'
        private static final byte DELAY_EIGHT = 0x38;        // '8'
    @VisibleForTesting
    static final byte TRIGGER_SOURCE          = 0x54;      // 'T'
                @VisibleForTesting
                static final byte TRIGGER_LED     = 0x31;    // '1'
                @VisibleForTesting
                static final byte TRIGGER_SPEAKER = 0x32;    // '2'
                @VisibleForTesting
                static final byte TRIGGER_BOTH    = 0x33;    // '3'
    private static final byte COMPETITION_MODE = 0x43;       // 'C'
        private static final byte COMPETITION_OFF    = 0x31; // '1'
        private static final byte COMPETITION_MASTER = 0x32; // '2'
        private static final byte COMPETITION_SLAVE  = 0x33; // '3'
    private static final byte SPEAKER_FREQ = 0x56;         // 'V'
        private static final byte FREQ_HALF     = 0x32;      // '2'
        private static final byte FREQ_FULL     = 0x34;      // '4'

    private static final byte DELAY_MIN_MIN = DELAY_ONE;
    private static final byte DELAY_MAX_MAX = DELAY_EIGHT;
    private static final byte DEFAULT_MIN_DELAY = DELAY_TWO;
    private static final byte DEFAULT_MAX_DELAY = DELAY_SEVEN;

    interface IBluetoothConnection {
        void connect(BluetoothDevice btDevice);
        void disconnect();
        boolean write(byte[] outputBuffer);
    }

    private final byte[] outputBuffer = new byte[4];
    @VisibleForTesting
    byte[] getOutputBuffer() { return outputBuffer; }

    private MainActivity mainActivity;
    private String btDeviceName;    // "MRTimer#"
    @VisibleForTesting
    int btConnectionState;

    private IBluetoothConnection btConnection;

    public TimerDriver() {
        if(DEBUG) { Log.d(TAG, "TimerDriver.ctor"); }
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    void connectToTimer(BluetoothDevice btDevice) {
        if(btConnection == null) {
            btConnection = new BluetoothConnection(this);
        } else {
            btConnection.disconnect();
        }
        btDeviceName = btDevice.getName();
        // If connected and then turnoff BT, btDevice != null but btDeviceName == null!
        if(btDeviceName != null) {
            Toast.makeText(mainActivity, "Connecting to " + btDeviceName, Toast.LENGTH_SHORT).show();
            btConnection.connect(btDevice);
        }
    }

    /**
     * Closes Bluetooth connection to <code>btDevice</code>.
     */
    void closeBtConnection() {
        if (btConnection != null) {
            btConnection.disconnect();
        }
        mainActivity.getPreferences(Context.MODE_PRIVATE).edit()
                .putString(CONNECTED_DEVICE, "") //getResources().getString(R.string.timer_not_connected))
                .putInt(TristateButton.TRISTATE, TRISTATE_TIMER_DISCONNECTED)
                .apply();
    }

    int getBtConnectionState() { return btConnectionState; }

    public void setBtConnectionState(int connectionState, int reason) {
        btConnectionState = connectionState;

        mainActivity.runOnUiThread(() -> {
            if ((reason == BT_PAIRED_DEVICE_UNAVAIL) || (reason == BT_CONNECTION_DEVICE_TURNED_OFF)) {
                if (!mainActivity.isFinishing()) {
                    Toast.makeText(mainActivity.getBaseContext(),
                            String.format(mainActivity.getString(R.string.handler_bt_unable_connect), btDeviceName),
                            Toast.LENGTH_SHORT).show();
                }
            }

            boolean isConnected = false;
            if (BluetoothConnection.BT_STATE_CONNECTED == connectionState) {
                isConnected = true;
                String deviceName = (btDeviceName == null) ? "" : btDeviceName;
                mainActivity.getPreferences(Context.MODE_PRIVATE).edit().putString(
                    CONNECTED_DEVICE, deviceName).apply();
            }
            mainActivity.onBtConnectionChanged(isConnected);
        });
    }

    public void setRawScore(byte[] dataPacket) {
        Score rawScore = Score.createScoreFromDataPacket(dataPacket);
        mainActivity.runOnUiThread( () -> mainActivity.finalizeScoreAndUpdate(rawScore) );
    }

    public void zeroScore() {
        mainActivity.runOnUiThread( mainActivity::zeroScore );
    }

    private boolean isBtConnectionReady() {
        if((btConnection != null) &&
                (btConnectionState == BluetoothConnection.BT_STATE_CONNECTED)) {
            clearBuffer();
            return true;
        } else {
            if (DEBUG) { Log.e(TAG, NOT_CONNECTED); }
            return false;
        }
    }

    /**
     * Sends "transmit score over Bluetooth" command to timer.
     * On success also sends "enable display".
     */
    boolean requestOutput() {
        if (isBtConnectionReady()) {
            outputBuffer[0] = ENABLE_OUTPUT;
            if (DEBUG) { Log.d(TAG, "requestOutput: " + new String(outputBuffer)); }

            if(btConnection.write(outputBuffer)) {
                outputBuffer[0] = ENABLE_DISPLAY; // FIRMWARE_VERSION; // 3 bytes, put breakpoint at IOThread.read: ZERO_APP_DISPLAY (diplays in decimal)
                if (DEBUG) { Log.d(TAG, "requestEnable: " + new String(outputBuffer)); }

                return btConnection.write(outputBuffer);
            }
        }
        return false;
    }

    /**
     * Note: a timer that's reset auto-starts after a random delay.
     *
     * @param shouldLatch true to latch display, false to reset/enable display
     */
    boolean latchOrResetDisplay(boolean shouldLatch) {
        if (isBtConnectionReady()) {
            outputBuffer[0] = shouldLatch ? LATCH_DISPLAY : ENABLE_DISPLAY;

            if (DEBUG) { Log.d(TAG, "latchOrResetDisplay: " + new String(outputBuffer)); }
            return btConnection.write(outputBuffer);
        }
        return false;
    }

    /**
     * @param trigger 1 for LED, 2 for speaker, 3 for both
     */
    boolean setTrigger(int trigger) {
        if (isBtConnectionReady()) {
            byte bTrigger = (byte) (TimerDriver.INT_TO_CHAR + trigger);
            outputBuffer[0] = TRIGGER_SOURCE;
            outputBuffer[1] =
                    ((bTrigger < TRIGGER_LED ) || (bTrigger > TRIGGER_BOTH)) ? TRIGGER_LED : bTrigger;

            if (DEBUG) { Log.d(TAG, "setTrigger: " + new String(outputBuffer)); }
            return btConnection.write(outputBuffer);
        }
        return false;
    }

    /**
     * Sends min/max trigger delay commands to timer.
     * Timer triggers at random delay between the two limits.
     * Delay is between 1 - 8 secs and minDelay <= maxDelay.
     *
     * @param minDelay between 1 - 8
     * @param maxDelay between 1 - 8
     */
    boolean setTriggerDelayLimits(int minDelay, int maxDelay) {
        if (isBtConnectionReady()) {
            int lMinDelay = (minDelay < 1) ? DEFAULT_MIN_DELAY : minDelay;
            int lMaxDelay = (maxDelay > 8) ? DEFAULT_MAX_DELAY : maxDelay;
            if(minDelay > maxDelay) {
                lMinDelay = DEFAULT_MIN_DELAY;
                lMaxDelay = DEFAULT_MAX_DELAY;
            }
            outputBuffer[0] = DELAYTIME_MIN;
            outputBuffer[1] = (byte) (INT_TO_CHAR + lMinDelay);
            outputBuffer[2] = DELAYTIME_MAX;
            outputBuffer[3] = (byte) (INT_TO_CHAR + lMaxDelay);

            if (DEBUG) { Log.d(TAG, "setTriggerDelayLimits: " + new String(outputBuffer)); }
            return btConnection.write(outputBuffer);
        }
        return false;
    }

    @VisibleForTesting
    void clearBuffer() {
        for(int i = 0; i < outputBuffer.length; i++) {
            outputBuffer[i] = DO_NOTHING;
        }
    }
}
