package work.johnmac.mrtimer.device;

interface ITimerResults {
    void setBtConnectionState(int connectionState, int reason);
    void zeroScore();
    void setRawScore(byte[] dataPacket);
}
