package work.johnmac.mrtimer.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import work.johnmac.mrtimer.BuildConfig;

/**
 * Represents a SQLite database containing Scores.
 *
 * @author JMac
 */
public final class SqlDbHelper extends SQLiteOpenHelper
                               implements ScoreInserter {

    static boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "SqlDbHelper";

    private static final String DB_NAME = "timerscoresdb.db";
    private static final int DB_VERSION = 3;
    static final String TABLE_NAME = "timerscores";
    private static final String DEFAULT_NAME = "Blank";

    public static final String ID = "_id";          // used by ScoreCursorAdapter.onBindViewHolder
    public static final String NAME = "localname";
    public static final String REACTION = "reaction";
    public static final String MOTION = "motion";
    public static final String RESPONSE = "response";
    public static final String RETRIEVED = "retrieved";           // date-time retrieved from timer in msec
    public static final String RETRIEVED_TIME = "retrieved_time"; // date-time retrieved in DISPLAY_DATE_FORMAT
    private static final String UPLOADED_FLAG = "uploaded_flag";  // true: successfully uploaded to host
    private static final String DELETE_FLAG = "delete_flag";      // true: delete locally & from host on sync

    private static final String CREATE_NAME_INDEX = "CREATE INDEX index_name ON "
            + TABLE_NAME + " (" + NAME + ");";
    private static final String CREATE_RETRIEVED_TIME_INDEX = "CREATE INDEX index_retrieved_time ON "
            + TABLE_NAME + " (" + RETRIEVED_TIME + ");";

    private static SqlDbHelper dbHelper;

    static synchronized SqlDbHelper getInstance(Context context) {
        if(dbHelper == null) {
            // Only access is through ContentProvider, which provides ApplicationContext.
            // To protect against future changes that might access this directly on a background
            // thread, grab ApplicationContext to prevent possible leaked Activity.
            dbHelper = new SqlDbHelper(context);//.getApplicationContext());
        }
        return dbHelper;
    }

    private SqlDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    /**
     * CREATE TABLE timer_scores
     * (_id INTEGER PRIMARY KEY AUTOINCREMENT,
     *  localname TEXT NOT NULL DEFAULT 'Anon',
     *  reaction INTEGER, motion INTEGER, response INTEGER NOT NULL,
     *  retrieved INTEGER NON NULL,
     *  retrieved_time DATE NON NULL,
     *  uploaded_flag INTEGER DEFAULT 0,
     *  delete_flag INTEGER DEFAULT 0);
     *
     * @param db The SQLite database that stores the timer scores
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        if (DEBUG) { Log.d(TAG, "onCreate() v" + DB_VERSION); }

        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                ID        + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                NAME      + " TEXT NOT NULL DEFAULT '" + DEFAULT_NAME + "', " +
                REACTION  + " INTEGER, " +
                MOTION    + " INTEGER, " +
                RESPONSE  + " INTEGER, " +
                RETRIEVED + " INTEGER NOT NULL, " +    // System.currentTimeMillis()
                RETRIEVED_TIME + " DATE NOT NULL DEFAULT 0, " +
                UPLOADED_FLAG  + " INTEGER DEFAULT 0, " +
                DELETE_FLAG    + " INTEGER DEFAULT 0);");

        // Must comment out when upgrading.
        db.execSQL(CREATE_NAME_INDEX);
        db.execSQL(CREATE_RETRIEVED_TIME_INDEX);

/*
        ContentValues cv = new ContentValues();
        cv.put(NAME, "B Lee");
        cv.put(REACTION, 123);
        cv.put(MOTION, 234);
        cv.put(RESPONSE, 357);
        long retrievedTime = System.currentTimeMillis();
        cv.put(RETRIEVED, retrievedTime);
        cv.put(RETRIEVED_TIME, DateConverter.convertRawToDisplayDate(retrievedTime));
        db.insert(TABLE_NAME, NAME, cv);
*/
    }

    // Must comment out index creation before upgrade.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (DEBUG) { Log.d(TAG, "onUpgrade() from v" + oldVersion + " to v" + newVersion); }

        String tempTableName = "temp";
        db.beginTransaction();
        try {
            db.execSQL("ALTER TABLE " + TABLE_NAME + " RENAME TO " + tempTableName);
            if (DEBUG) { Log.d(TAG, "onUpgrade(): ALTER TABLE completed successfully"); }
            onCreate(db);   // creates new TABLE_NAME
            if (DEBUG) { Log.d(TAG, "onUpgrade(): onCreate() completed successfully"); }

            insertPreviousData(db, tempTableName);

            db.execSQL("DROP TABLE IF EXISTS " + tempTableName);
            if (DEBUG) { Log.d(TAG, "onUpgrade(): DROP TABLE completed successfully"); }
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            if (DEBUG) { Log.e(TAG, "onUpgrade() caught SQLException, changes rolled back", e); }
        } finally { db.endTransaction(); }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (DEBUG) { Log.e(TAG, "onUpgrade() from v" + oldVersion + " to v" + newVersion); }
        throw new SQLiteException("Can't downgrade database from version " +
                oldVersion + " to " + newVersion);
    }

    // From v2 to v3 (_87).
    private void insertPreviousData(SQLiteDatabase db, String tempTableName) {
        String columns = NAME + ',' + REACTION + ',' + MOTION + ',' + RESPONSE + ',' + RETRIEVED + ',' + RETRIEVED_TIME;
        String orderBy = " ORDER BY " + RETRIEVED + " ASC";
        // INSERT INTO timerscores(localname,reaction,motion,response,retrieved,retrieved_time) SELECT localname,reaction,motion,response,retrieved,retrieved_time FROM temp ORDER BY retrieved ASC
        String sqlString = "INSERT INTO " + TABLE_NAME + '(' + columns + ')' +
                " SELECT " + columns + " FROM " + tempTableName + orderBy;
        db.execSQL(sqlString);

        if (DEBUG) { Log.d(TAG, "onUpgrade(): INSERT completed successfully"); }
    }

    // SQLiteDatabase internally creates a transaction for each statement.
    @Override
    public long insert(ContentValues cv) {
        return getWritableDatabase().insert(TABLE_NAME, null, cv);
    }
}
