package work.johnmac.mrtimer.model;

import android.content.ContentValues;

/**
 * DB insert.
 */
interface ScoreInserter {
    long insert(ContentValues cv);
}
