package work.johnmac.mrtimer.model;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import work.johnmac.mrtimer.BuildConfig;

import static android.content.ContentResolver.CURSOR_DIR_BASE_TYPE;
import static android.content.ContentResolver.CURSOR_ITEM_BASE_TYPE;
import static work.johnmac.mrtimer.model.SqlDbHelper.*;

/**
 * Manages access to a SQLite database.
 * Allows use of a <code>CursorLoader</code> in ScoresFragment.
 * (Loader accesses database on background thread)
 *
 * <code>BluetoothConnection.IOThread</code> retains an option to
 * directly write new scores to the database.
 *
 * @author JMac
 */
public final class ScoreProvider extends ContentProvider {
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "ScoreProvider";

    /**
     * Max # of scores to retrieve.
     */
    private static final String LIMIT = "500";

    private static final String[] DEFAULT_PROJECTION = {
            ID, // for ScoreCursorAdapter
            NAME, REACTION, MOTION, RESPONSE, RETRIEVED_TIME
    };

    private static final String SCHEME = "content://";
    static final String AUTHORITY = BuildConfig.APPLICATION_ID + ".provider";

    // BASE_PATH required for UriMatcher.match.
    // In UriMatcher.match: '#' is placeholder for int/long, '*' placeholder for text
    private static final String BASE_PATH = TABLE_NAME;
    private static final String BASE_PATH_ID = BASE_PATH + "/#";

    // By default, retrieves DEFAULT_PROJECTION columns
    public static final Uri SCORES_URI = Uri.parse(SCHEME + AUTHORITY + '/' + BASE_PATH);
    public static final Uri SCORE_URI  = Uri.parse(SCHEME + AUTHORITY + '/' + BASE_PATH_ID);
    static final String UNSUPPORTED_URI = "Unsupported URI: ";

    private static final int SCORES = 1;
    private static final int SCORE = 2;
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, SCORES);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH_ID, SCORE);
    }

    static final String MIME_SCORES = CURSOR_DIR_BASE_TYPE  + "/vnd.work.johnmac.mrtimer.scores";
    static final String MIME_SCORE  = CURSOR_ITEM_BASE_TYPE + "/vnd.work.johnmac.mrtimer.score";

    private SqlDbHelper dbHelper;

    @Override
    public boolean onCreate() {
        if (DEBUG) { Log.d(TAG, "onCreate()"); }

        // Content providers are created on application main thread at application launch time.
        dbHelper = getInstance(getContext()); // ApplicationContext
        return true;
    }

    // For use with testing. See ContentProvider.shutdown doc.
    @Override
    public void shutdown() {
        dbHelper.close();
    }

    // Not used
    @Override
    public String getType(@NonNull Uri uri) {
        if(DEBUG) { Log.d(TAG, "getType(), uri: " + uri); }
        int matchType = sUriMatcher.match(uri);

        switch(matchType) {
            case SCORES:
                return MIME_SCORES;
            case SCORE:
                return MIME_SCORE;
            default:
                if(DEBUG) {
                    Log.e(TAG, "getType(): " + UNSUPPORTED_URI);
                }
                return UNSUPPORTED_URI;
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues cv) {
        if (DEBUG) { Log.d(TAG, "insert()"); }

        long id = dbHelper.getWritableDatabase().insert(TABLE_NAME, null, cv);
        Uri newUri = ContentUris.withAppendedId(uri, id);
        getContext().getContentResolver().notifyChange(newUri, null);
        return newUri;
    }

    /**
     * Returns all rows up to LIMIT (500, not modifiable by client).
     *
     * @param uri SCORES_URI or SCORE_URI
     * @param projection optional array of column names to return
     * @param selection optional WHERE clause.
     * @param selectionArgs optional
     * @param sortOrder optional
     * @return all rows satisfying the query
     */
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection,
                        @Nullable String selection, @Nullable String[] selectionArgs,
                        @Nullable String sortOrder) {
        if (DEBUG) { Log.d(TAG, "query()"); }

        if( (sUriMatcher.match(uri) != SCORES) && (sUriMatcher.match(uri) != SCORE) ) {
            throw new IllegalArgumentException(UNSUPPORTED_URI + uri);
        }

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(TABLE_NAME);

        String orderBy = (sortOrder == null) ? ID + " desc" : sortOrder;
        String[] queryProjection = (projection == null) ? DEFAULT_PROJECTION : projection;

        Cursor curse;
        if (sUriMatcher.match(uri) == SCORE) {
            String id = uri.getLastPathSegment();
            if(isIdAPositiveLong(id)) {
                queryBuilder.appendWhere(ID + '=' + id);
            }
        }
        curse = queryBuilder.query(dbHelper.getReadableDatabase(), queryProjection,
                selection, selectionArgs, null, null, orderBy, LIMIT);

        curse.setNotificationUri(getContext().getContentResolver(), uri);
        return curse;
    }

    // Not used
    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        if (DEBUG) { Log.d(TAG, "update()"); }
        return -1;
    }

    /**
     * Delete row(s)
     *
     * @param uri SCORES_URI or SCORE_URI
     * @param selection optional for SCORES_URI, ignored for SCORE_URI
     * @param selectionArgs optional
     * @return number of rows deleted
     */
    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        if (DEBUG) { Log.d(TAG, "delete: " + selection); }

        if( (sUriMatcher.match(uri) != SCORES) && (sUriMatcher.match(uri) != SCORE) ) {
            throw new IllegalArgumentException(UNSUPPORTED_URI + uri);
        }

        int rowsDeleted = 0;
        if (sUriMatcher.match(uri) == SCORE) {
            String id = uri.getLastPathSegment();
            if(isIdAPositiveLong(id)) {
                rowsDeleted = dbHelper.getWritableDatabase().delete(
                        TABLE_NAME, ID + '=' + id, null);
            }
        } else {
            rowsDeleted = dbHelper.getWritableDatabase().delete(
                    TABLE_NAME, selection, selectionArgs);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @VisibleForTesting
    boolean isIdAPositiveLong(String id) {
        try {
            if(Long.parseLong(id) > 0) {
                return true;
            }
        } catch (NumberFormatException e) {
            if (DEBUG) {
                Log.e(TAG, "query(): NumberFormatException, id = " + id);
            }
        }
        return false;
    }
}
