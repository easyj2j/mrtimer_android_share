package work.johnmac.mrtimer.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.Log;

import work.johnmac.mrtimer.BuildConfig;
import work.johnmac.mrtimer.utility.DateConverter;

import static work.johnmac.mrtimer.model.SqlDbHelper.*;
import static work.johnmac.mrtimer.utility.DateConverter.DISPLAY_DATE_FORMAT;
import static work.johnmac.mrtimer.utility.DateConverter.MSEC_PRECISION_LOSS;

/**
 * Represents an immutable Score retrieved from an external timing device.
 *
 * When a Score is initially created from the timer data,
 * localID and localName are set to default values.
 * Later, localID is set from the Db insertion and localName from preferences.
 * This is performed by returning a new Score created from the initial Score.
 */
public final class Score implements Parcelable {
    static boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "Score";

    private static final int OVERFLOW = 9999;

    static final long DEFAULT_LOCALID = -1;
    static final String DEFAULT_LOCALNAME = "Anon";

    private final long localID;
    private final String localName;
    private final int reaction;
    private final int motion;
    private final int response;
    // date-time retrieved from timer in msec but with only seconds precision
    private final long retrieved;

    public long getID() { return localID; }
    public String getName() { return localName; }
    public int getReaction() { return reaction; }
    public int getMotion() { return motion; }
    public int getResponse() { return response; }
    public long getRetrieved() { return retrieved; }

    /**
     * Converts a data packet retrieved from a timer into a Score.
     * One data packet: !&motn&resp\n
     *   '!' & '\n ' are packet delimiters.
     *   '&' denotes beginning of a msec time value (char format).
     *   ie: !&0132&0259\n
     *
     * Should only be called by BluetoothConnection & tests.
     *
     * @param inputBuffer data packet from timer
     *
     * @return a Score (retrieved = 0 indicates a bad score)
     */
    public static Score createScoreFromDataPacket(byte[] inputBuffer) {
        int reaction, response, motion;
        try {
            // Throw away bytes 0,1,6,11.
            reaction = Integer.parseInt(new String(inputBuffer, 2, 4));
            response = Integer.parseInt(new String(inputBuffer, 7, 4));
            motion = (0 == reaction) ? 0 : response - reaction;
        } catch (NumberFormatException nfe) {
            // Bad score (parseInt) from timer: return retrieved = 0.
            if (DEBUG) { Log.e(TAG, "IOThread.read(): bad score from timer", nfe); }
            return new Score(DEFAULT_LOCALID, DEFAULT_LOCALNAME, 0, 0, 0, 0);
        }

        // Counter overflow - probably just running: return retrieved = 0.
        if ((OVERFLOW == reaction) || (OVERFLOW == response)) {
            return new Score(DEFAULT_LOCALID, DEFAULT_LOCALNAME, 0, 0, 0, 0);
        }

        long retrieved = //System.currentTimeMillis();
            (System.currentTimeMillis()/MSEC_PRECISION_LOSS) * MSEC_PRECISION_LOSS;

        return new Score(DEFAULT_LOCALID, DEFAULT_LOCALNAME, reaction, motion, response, retrieved);
    }

    /**
     * Called by ScoreJson
     */
    public Score(long id, String name, int reaction, int motion, int response, long retrieved) {
        localID = id;
        localName = name;
        this.reaction = reaction;
        this.motion = motion;
        this.response = response;
        // Remove msec precision as it's unused but screws up date conversions
        this.retrieved = (retrieved/MSEC_PRECISION_LOSS) * MSEC_PRECISION_LOSS;
    }

    /**
     * Called by ScoresFragment.queryAndSetAdapter
     *
     * @param cursor default ScoreProvider.query
     */
    public Score(Cursor cursor) {
        this.localID = cursor.getLong(cursor.getColumnIndex(ID));
        this.localName = cursor.getString(cursor.getColumnIndex(NAME));
        this.reaction = cursor.getInt(cursor.getColumnIndex(REACTION));
        this.motion = cursor.getInt(cursor.getColumnIndex(MOTION));
        this.response = cursor.getInt(cursor.getColumnIndex(RESPONSE));
        this.retrieved = DateConverter.convertDisplayDateToRaw(
                cursor.getString(cursor.getColumnIndex(RETRIEVED_TIME)));
    }

    /**
     * Create Score from ContentValues.
     * Called by InsertScoreTask
     */
    public Score(ContentValues cv) {
        localID = (long)cv.get(ID);
        localName = (String)cv.get(NAME);
        this.reaction = (int)cv.get(REACTION);
        this.motion = (int)cv.get(MOTION);
        this.response = (int)cv.get(RESPONSE);
        // Remove msec precision as it's unused but screws up date conversions
        this.retrieved = ((long)cv.get(RETRIEVED)/MSEC_PRECISION_LOSS) * MSEC_PRECISION_LOSS;
    }

    /**
     * @return ContentValues used by ScoreProvider.insert for Db insertion
     */
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues(6);
        cv.put(NAME, localName);
        cv.put(REACTION, reaction);
        cv.put(MOTION, motion);
        cv.put(RESPONSE, response);
        cv.put(RETRIEVED, retrieved);
        // Process here as it's only needed on db.insert
        cv.put(RETRIEVED_TIME, DISPLAY_DATE_FORMAT.format(retrieved));
        return cv;
    }

    //********** Parcelable **********/
    private Score(Parcel in) {
        localID = in.readLong();
        localName = in.readString();
        reaction = in.readInt();
        motion = in.readInt();
        response = in.readInt();
        retrieved = in.readLong();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(localID);
        dest.writeString(localName);
        dest.writeInt(reaction);
        dest.writeInt(motion);
        dest.writeInt(response);
        dest.writeLong(retrieved);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Score> CREATOR = new Creator<Score>() {
        @Override
        public Score createFromParcel(Parcel in) {
            return new Score(in);
        }

        @Override
        public Score[] newArray(int size) {
            return new Score[size];
        }
    };
    //********************************/

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Score)) { return false; } //  instanceof checks for obj == null
        if(obj == this) { return true; }

        Score score = (Score)obj;
        return (score.localID == localID) &&
               (score.localName.equals(localName)) &&
               (score.reaction == reaction) &&
               (score.motion == motion) &&
               (score.response == response) &&
               (score.retrieved == retrieved);
    }

    @Override
    public int hashCode() {
        int result = 13;
        result = 31 * result + (int) (localID ^ (localID >>> 32));
        result = 31 * result + localName.hashCode();
        result = 31 * result + reaction;
        result = 31 * result + response;
        result = 31 * result + (int)(retrieved ^ (retrieved >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return localName +
                " '" + reaction + " + " + motion + " = " + response + "' '" +
                DISPLAY_DATE_FORMAT.format(retrieved) + "'";
    }
}
