package work.johnmac.mrtimer;

import android.app.Application;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.squareup.leakcanary.LeakCanary;
import dagger.Lazy;
import javax.inject.Inject;

import work.johnmac.mrtimer.device.BluetoothValidator;
import work.johnmac.mrtimer.device.TimerDeviceService;
import work.johnmac.mrtimer.di.AppComponent;
import work.johnmac.mrtimer.di.BluetoothModule;
import work.johnmac.mrtimer.di.DaggerAppComponent;
import work.johnmac.mrtimer.di.RestModule;
import work.johnmac.mrtimer.rest.RestClient;

public class App extends Application {
    //private static final String HOST_URL = "http://mrtimer.azurewebsites.net/";

    private AppComponent appComponent;

    // Uploading a new score is an optional setting.
    @Inject Lazy<RestClient> restClient;
    @Inject TimerDeviceService timerDeviceService;
    @Inject BluetoothValidator bluetoothValidator;

    @Override
    public void onCreate() {
//        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                .detectDiskReads()
//                .detectDiskWrites()
//                .detectNetwork()   // or .detectAll() for all detectable problems
//                .penaltyLog()
//                .build());
//        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                .detectLeakedSqlLiteObjects()
//                .detectLeakedClosableObjects()
//                .penaltyLog()
//                .penaltyDeath()
//                .build());
        super.onCreate();

        // This process dedicated to LeakCanary for heap analysis; don't init app in this process.
//        if (LeakCanary.isInAnalyzerProcess(this)) { return; }
//        LeakCanary.install(this);
//        Stetho.initializeWithDefaults(this);

        appComponent = DaggerAppComponent.builder()
                .restModule(new RestModule(getHostUrl()))
                .bluetoothModule(new BluetoothModule())
                .build();
        appComponent.inject(this); // (lazily) injects restClient
    }

    // Retrieves Url specified in application.meta-data element of manifest.
    public String getHostUrl() {
        String url = "";
        ApplicationInfo appInfo;
        try {
            appInfo = getPackageManager().getApplicationInfo(
                    getPackageName(), PackageManager.GET_META_DATA);
            url = appInfo.metaData.getString("hostUrl");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return url;
    }

    public RestClient getRestClient() {
        return restClient.get();
    }

    public TimerDeviceService getTimerDeviceService() {
        return timerDeviceService;
    }

    public BluetoothValidator getBluetoothValidator() {
        return bluetoothValidator;
    }
}
