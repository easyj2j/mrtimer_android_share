package work.johnmac.mrtimer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import work.johnmac.mrtimer.databinding.FragmentScoreBinding;
import work.johnmac.mrtimer.model.Score;

import static work.johnmac.mrtimer.model.SqlDbHelper.MOTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.REACTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.RESPONSE;
import static work.johnmac.mrtimer.utility.DateConverter.formatIntAsString;
import static work.johnmac.mrtimer.utility.TristateButton.TRISTATE_TIMER_DISCONNECTED;
import static work.johnmac.mrtimer.utility.TristateButton.TRISTATE;

/**
 * Displays a score retrieved from the timer device.
 * Can connect to timer and latch/re-enable the timer display.
 */

public final class ScoreFragment extends Fragment {
    interface TristateListener {
        void onTristateChanged();
    }
    private TristateListener tristateListener;

    // Preferences
    public static final String SELECTED_USER  = "selectedUser";                  // string
    private static final String SELECTED_USER_POSITION = "selectedUserPosition"; // int

    private static final String ZERO = "0.000";

    private FragmentScoreBinding dataBindings;
    private SharedPreferences preferences;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setRetainInstance(true);

        try {
            tristateListener = (TristateListener)context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement TristateListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        preferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        dataBindings = FragmentScoreBinding.inflate(inflater);
        dataBindings.setEventHandlers(this);

        if(savedInstanceState != null) {
            dataBindings.reactionView.setText(savedInstanceState.getCharSequence(REACTION, ZERO));
            dataBindings.motionView.setText(savedInstanceState.getCharSequence(MOTION, ZERO));
            dataBindings.responseView.setText(savedInstanceState.getCharSequence(RESPONSE, ZERO));
        }

        dataBindings.tristateButton.setState(preferences.getInt(
                TRISTATE, TRISTATE_TIMER_DISCONNECTED));
        return dataBindings.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        dataBindings.userSpinner.setAdapter(new ArrayAdapter<>(
                getActivity(), R.layout.spinner_item,
                getActivity().getResources().getStringArray(R.array.users_noContacts)));
    }

    @Override
    public void onStart() {
        super.onStart();
        setSelectedUserNameFromPreferences();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putCharSequence(REACTION, dataBindings.reactionView.getText());
        outState.putCharSequence(MOTION, dataBindings.motionView.getText());
        outState.putCharSequence(RESPONSE, dataBindings.responseView.getText());
    }

    // R.id.user_spinner
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.equals(dataBindings.userSpinner)) {
            String selectedUser = (String)parent.getItemAtPosition(position);
            preferences.edit().putString(SELECTED_USER, selectedUser)
                    .putInt(SELECTED_USER_POSITION, position).apply();
        }
    }

    /**
     * @param v TristateButton
     */
    public void onClick(View v) {
        int tristate = dataBindings.tristateButton.getState();
        preferences.edit().putInt(TRISTATE, tristate).apply();
        tristateListener.onTristateChanged();
    }

    void setTristate() {
        int tristate = preferences.getInt(TRISTATE, TRISTATE_TIMER_DISCONNECTED);
        dataBindings.tristateButton.setState(tristate);
    }

    /**
     * Zeros the score in response to notification from the timer .
     */
    void zeroScore() {
        dataBindings.reactionView.setText(ZERO);
        dataBindings.motionView.setText(ZERO);
        dataBindings.responseView.setText(ZERO);
    }

    /**
     * Updates the UI for a new score.
     *
     * @param newScore the new Score
     */
    void updateScore(Score newScore) {
        dataBindings.reactionView.setText(formatIntAsString(newScore.getReaction()));
        dataBindings.motionView.setText(formatIntAsString(newScore.getMotion()));
        dataBindings.responseView.setText(formatIntAsString(newScore.getResponse()));
    }

    private void setSelectedUserNameFromPreferences() {
        int selectedUserPosition = preferences.getInt(SELECTED_USER_POSITION, 0);
        dataBindings.userSpinner.setSelection(selectedUserPosition, true);
    }
}
