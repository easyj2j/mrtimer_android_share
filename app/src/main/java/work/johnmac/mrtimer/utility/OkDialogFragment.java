package work.johnmac.mrtimer.utility;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;

import work.johnmac.mrtimer.R;

/**
 * An <code>AlertDialog</code> with a single 'OK' button.
 * A modal alternative to a <code>Toast</code>.
 *
 * @author JMac
 */
public class OkDialogFragment extends DialogFragment {
    private String message;

    @Override
    public @NonNull Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) { message = savedInstanceState.getString("message"); }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.app_name))
                .setIcon(android.R.drawable.ic_menu_info_details)
                .setNeutralButton("OK", null)
                .setMessage(message);
        return builder.create();
    }

    public OkDialogFragment setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString("message", message);
    }
}
