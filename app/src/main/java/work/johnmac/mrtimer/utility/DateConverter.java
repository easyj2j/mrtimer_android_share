package work.johnmac.mrtimer.utility;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import work.johnmac.mrtimer.BuildConfig;

import static java.util.Locale.US;

public final class DateConverter {
    static boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = DateConverter.class.getSimpleName();
    public static final long MSEC_PRECISION_LOSS = 1000;
    public static final long SEC_PRECISION_LOSS = 60000;

    private DateConverter() {}

    public static String formatIntAsString(Integer i) {
        return String.format(US, "%4.3f", .001 * i);
    }

    // Remove millisecond precision since it's not used by ReST_DATE_FORMAT
    // but does screw up date conversions - see Score(int, ...).
    public static long setRawTime() {
        return (System.currentTimeMillis()/MSEC_PRECISION_LOSS) * MSEC_PRECISION_LOSS;
    }

    // TODO: replace w static Java8 DateTimeFormatter when 'O' (api26) releases.
    // NOTE: icu.SimpleDateFormat available in api24
    public static final SimpleDateFormat ReST_DATE_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat DISPLAY_DATE_FORMAT =
            new SimpleDateFormat("MM/dd/yy hh:mm a");

    // Remove seconds precision since it's not used by DISPLAY_DATE_FORMAT
    // but does screw up date conversions.
    public static String convertRawToDisplayDate(long raw) {
        String displayDate = DISPLAY_DATE_FORMAT.format((raw/SEC_PRECISION_LOSS) * SEC_PRECISION_LOSS);
        return displayDate;
    }

    public static long convertDisplayDateToRaw(String displayDate) {
        long displayDateDeserialized = 0;
        try {
            displayDateDeserialized = DISPLAY_DATE_FORMAT.parse(displayDate).getTime();
        } catch (ParseException e) {
            if(DEBUG) { Log.e(TAG, "convertDisplayDateToRaw: ", e); }
        }
        return displayDateDeserialized;
    }

    // Gson serialize
    public static String convertRawToReSTDate(long raw) {
        String reST = ReST_DATE_FORMAT.format(raw);
        return reST;
    }

    // Gson deserialize
    public static long convertReSTDateToRaw(String reSTDate) {
        String[] split = reSTDate.split("T", 2);
        String dateTime = (split.length == 1) ? split[0] : split[0] + ' ' + split[1];
        long reSTDeserialized = 0;
        try {
            reSTDeserialized = ReST_DATE_FORMAT.parse(dateTime).getTime();
        } catch (ParseException e) {
            if(DEBUG) { Log.e(TAG, "convertReSTToRawDate: ", e); }
        }
        return reSTDeserialized;
    }
}
