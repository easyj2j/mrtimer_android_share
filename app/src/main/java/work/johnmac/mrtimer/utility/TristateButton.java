package work.johnmac.mrtimer.utility;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.Log;

import work.johnmac.mrtimer.BuildConfig;
import work.johnmac.mrtimer.R;

/**
 * A 3-state button that sends commands to the timer device.<br>
 * States:
 * <ul>
 * <li>Disconnected</li>
 * <li>Latched</li>
 * <li>Reset</li>
 * </ul>
 *
 * @author JMac
 */
public class TristateButton extends AppCompatButton {
    // Debugging constants.
    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final String TAG = "TristateButton";

    public static final String TRISTATE = "tristate";
    /**
     * App is not connected to timer.
     */
    public static final int TRISTATE_TIMER_DISCONNECTED = 1;
    /**
     * Timer display is frozen.
     */
    public static final int TRISTATE_TIMER_LATCHED = 2;
    /**
     * Timer display is reset at zero or counting.
     * After a reset there's a random delay then counting starts.
     */
    public static final int TRISTATE_TIMER_RESET = 3;

    private int state = TRISTATE_TIMER_DISCONNECTED;

    public TristateButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (DEBUG) { Log.d(TAG, "ctor()"); }
    }

    public TristateButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TristateButton(Context context) {
        this(context, null, 0);
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        if (DEBUG) { Log.d(TAG, "setState(): " + state); }

        this.state = state;

        // When in latched state, show reset, & vice versa
        switch (state) {
            case TRISTATE_TIMER_DISCONNECTED:
                setBackgroundResource(R.drawable.button_notconnected);
                break;
            case TRISTATE_TIMER_LATCHED:
                setBackgroundResource(R.drawable.button_reset);
                break;
            case TRISTATE_TIMER_RESET:
                setBackgroundResource(R.drawable.button_latch);
                break;
            default:
                Log.e(TAG, "setState, unknown state: " + state);
        }
    }
}
