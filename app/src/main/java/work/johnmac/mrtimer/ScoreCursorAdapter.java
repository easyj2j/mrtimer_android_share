package work.johnmac.mrtimer;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import work.johnmac.mrtimer.databinding.ScoresListItemBinding;
import work.johnmac.mrtimer.model.Score;
import work.johnmac.mrtimer.utility.DateConverter;

import static work.johnmac.mrtimer.utility.DateConverter.formatIntAsString;

/**
 * Adapter for <code>ScoresFragment</code>.
 */
final class ScoreCursorAdapter extends RecyclerView.Adapter<ScoreCursorAdapter.RowHolder> {
    private final List<Score> scoresList;

    // Row colors
    private final int white, alternate, highlight;

    // For highlighting
    private int newScoreId = -1;
    void setNewScoreId(long id) { newScoreId = (int)id; }

    // getColor(int) deprecated in API 23 for getColor(int, Theme); theme may = null
    @SuppressWarnings("deprecation")
    ScoreCursorAdapter(Context context, List<Score> scoresList) {
        this.scoresList = scoresList;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            white = context.getResources().getColor(android.R.color.white, context.getTheme());
            alternate = context.getResources().getColor(R.color.goldenrod_lite, null);
            highlight = context.getResources().getColor(R.color.goldenrod, null);
        } else {
            white = context.getResources().getColor(android.R.color.white);
            alternate = context.getResources().getColor(R.color.goldenrod_lite);
            highlight = context.getResources().getColor(R.color.goldenrod);
        }
    }

    @Override
    public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View rowView = inflater.inflate(R.layout.scores_list_item, parent, false);
        return new RowHolder(rowView);
    }

    /**
     * Provides different colors for alternate rows & highlights a new score.
     */
    @Override
    public void onBindViewHolder(RowHolder holder, int position) {
        holder.rowView.setBackgroundColor( (position % 2) == 1 ? white : alternate );

        Score score = scoresList.get(position);
        if (score.getID() == newScoreId) {
            holder.rowView.setBackgroundColor(highlight);
        }
        holder.dataBindings.listName.setText(score.getName());
        holder.dataBindings.listReaction.setText(formatIntAsString(score.getReaction()));
        holder.dataBindings.listMotion.setText(formatIntAsString(score.getMotion()));
        holder.dataBindings.listResponse.setText(formatIntAsString(score.getResponse()));
        holder.dataBindings.listDate.setText(
                DateConverter.convertRawToDisplayDate(score.getRetrieved()));
        holder.dataBindings.executePendingBindings();
    }

    @Override
    public int getItemCount() { return scoresList.size(); }

    static class RowHolder extends RecyclerView.ViewHolder {
        private final ScoresListItemBinding dataBindings;
        private final View rowView;

        RowHolder(View rowView) {
            super(rowView);
            this.rowView = rowView;
            dataBindings = ScoresListItemBinding.bind(rowView);
        }
    }
}
