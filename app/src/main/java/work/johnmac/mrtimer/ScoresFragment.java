package work.johnmac.mrtimer;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import work.johnmac.mrtimer.databinding.FragmentScoresBinding;
import work.johnmac.mrtimer.model.Score;

import static android.content.Context.MODE_PRIVATE;
import static android.graphics.Typeface.DEFAULT;
import static android.graphics.Typeface.ITALIC;
import static android.graphics.Typeface.NORMAL;
import static work.johnmac.mrtimer.model.ScoreProvider.SCORES_URI;
import static work.johnmac.mrtimer.model.SqlDbHelper.MOTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.NAME;
import static work.johnmac.mrtimer.model.SqlDbHelper.REACTION;
import static work.johnmac.mrtimer.model.SqlDbHelper.RESPONSE;
import static work.johnmac.mrtimer.model.SqlDbHelper.RETRIEVED;

/**
 * Displays a multicolumn list of scores with alternating row colors.
 * Includes a date column in landscape mode.
 * Binds to a <code>SQLiteDatabase</code> through a custom <code>SimpleCursorAdapter</code>.
 * Uses a RecyclerView and the DataBinding library.
 *
 * @author JMac
 */
public final class ScoresFragment extends Fragment {

    // Parcelable
    private static final String SCORES_LIST = "scoresList";

    // SharedPreferences
    private static final String PREV_SORT_COLUMN = "prevSortColumn";
    private static final String IS_SORT_ASC = "isSortAsc";
    private static final String ORDER_BY = "orderBy";
    private static final String DEFAULT_ORDER_BY = RETRIEVED + " DESC";

    private SharedPreferences preferences;
    private FragmentScoresBinding dataBindings;
    private List<Score> scoresList;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        preferences = getActivity().getPreferences(MODE_PRIVATE);
        dataBindings = FragmentScoresBinding.inflate(inflater);
        dataBindings.scoresListHeader.setEventHandlers(this);

        // Hack to only show Date column in landscape mode
        dataBindings.scoresListHeader.listDate.setVisibility(
                getContext().getResources().getBoolean(R.bool.is_portrait) ?
                        View.GONE : View.VISIBLE);
        dataBindings.scoresList.setLayoutManager(new LinearLayoutManager(getActivity()));
        return dataBindings.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null) {
            scoresList = savedInstanceState.getParcelableArrayList(SCORES_LIST);
            setAdapter(scoresList);
        } else {
            queryAndSetAdapter(preferences.getString(ORDER_BY, DEFAULT_ORDER_BY), false, 0);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(SCORES_LIST, (ArrayList<Score>)scoresList);
        super.onSaveInstanceState(outState);
    }

    /**
     * Sorts list by column, alternating direction for contiguous clicks on a single column.
     * Sorts by querying Db.
     * Italicizes the header of the sorted column.
     *
     * @param newSortHeader TextView in header representing column to sort on: R.id.list_x
     */
    public void onClick(View newSortHeader) {
        switchItalicizedHeader((TextView)newSortHeader);
        String sortColumn = columnViewToString(newSortHeader);
        queryAndSetAdapter(createOrderBy(sortColumn), false, 0);
    }

    /**
     * Hack to sort by Retrieved/Date in portrait mode which doesn't display date column.
     *
     * @return true
     */
    public boolean onLongClick(View notUsed) {
        switchItalicizedHeader(dataBindings.scoresListHeader.listDate);

        queryAndSetAdapter(createOrderBy(RETRIEVED), false, 0);
        return true;
    }

    private void switchItalicizedHeader(TextView newSortHeader) {
        TextView prevSortHeader = columnStringToView(
                preferences.getString(PREV_SORT_COLUMN, RETRIEVED));
        prevSortHeader.setTypeface(DEFAULT, NORMAL);
        newSortHeader.setTypeface(DEFAULT, ITALIC);
    }

    private String columnViewToString(View columnView) {
        String columnString;
        switch (columnView.getId()) {
            case R.id.list_name:
                columnString = NAME;
                break;
            case R.id.list_reaction:
                columnString = REACTION;
                break;
            case R.id.list_motion:
                columnString = MOTION;
                break;
            case R.id.list_response:
                columnString = RESPONSE;
                break;
            default:
                columnString = RETRIEVED;
                break;
        }
        return columnString;
    }

    private TextView columnStringToView(String columnString) {
        TextView columnView;
        switch (columnString) {
            case NAME:
                columnView = dataBindings.scoresListHeader.listName;
                break;
            case REACTION:
                columnView = dataBindings.scoresListHeader.listReaction;
                break;
            case MOTION:
                columnView = dataBindings.scoresListHeader.listMotion;
                break;
            case RESPONSE:
                columnView = dataBindings.scoresListHeader.listResponse;
                break;
            default:
                columnView = dataBindings.scoresListHeader.listDate;
                break;
        }
        return columnView;
    }

    /**
     * Creates SQL ORDER BY clause for sorting scores.
     *
     * @param newSortColumn column to sort by
     * @return ORDER BY clause
     */
    private String createOrderBy(String newSortColumn) {
        String prevSortColumn = preferences.getString(PREV_SORT_COLUMN, RETRIEVED);
        boolean isSortAsc = preferences.getBoolean(IS_SORT_ASC, false);

        // If sort on Name, also sort on Retrieved date.
        String sortByDate = (newSortColumn.equals(NAME)) ? ", " + DEFAULT_ORDER_BY : " ";

        // Initially sorted by Retrieved/Date.
        // 1st sort on any column except Retrieved/Date is ASC (true).
        // Sorts in alternating direction for contiguous clicks on a single column.
        isSortAsc = newSortColumn.equals(prevSortColumn) ?
                !isSortAsc : !newSortColumn.equals(RETRIEVED);

        prevSortColumn = newSortColumn;

        // SQL OrderBy clause.
        String orderBy = newSortColumn + (isSortAsc ? " ASC" : " DESC") + sortByDate;

        preferences.edit()
                .putString(PREV_SORT_COLUMN, prevSortColumn)
                .putBoolean(IS_SORT_ASC, isSortAsc)
                .putString(ORDER_BY, orderBy).apply();

        return orderBy;
    }

    /**
     * Prepends a new score to scoresList.
     * Highlights the new entry.
     * Sets the view to top of scoresList.
     *
     * @param newScore the new Score
     */
    void updateScore(Score newScore) {//long newId) {
//        queryAndSetAdapter(DEFAULT_ORDER_BY, true, newId);

        // Appends to list, which places it on top
        scoresList.add(0, newScore);
        setNewId(newScore.getID());
        dataBindings.scoresList.getAdapter().notifyDataSetChanged();
    }

    /**
     * Background queries the DB for all scores and sets the result to the Adapter.
     */
    private void queryAndSetAdapter(String orderBy, boolean isUpdate, long newId) {

        new AsyncTask<Object, Void, List<Score>>() {
            @Override
            protected List<Score> doInBackground(Object... params) {
                List<Score> scoresList;
                Cursor cursor = getContext().getContentResolver().query(
                        SCORES_URI, null, null, null, orderBy);
                if(cursor == null) {
                    return new ArrayList<>(0);
                }

                scoresList = new ArrayList<>(cursor.getCount());
                while(cursor.moveToNext()) {
                    scoresList.add(new Score(cursor));
                }
                cursor.close();
                return scoresList;
            }

            @Override
            protected void onPostExecute(List<Score> scoresList) {
                setAdapter(scoresList);
                if (isUpdate) { setNewId(newId); }
            }
        }.execute();
    }

    private void setAdapter(List<Score> scoresList) {
        this.scoresList = scoresList;
        RecyclerView.Adapter<ScoreCursorAdapter.RowHolder> adapter =
                new ScoreCursorAdapter(getActivity(), scoresList);
        dataBindings.scoresList.setAdapter(adapter);
        dataBindings.scoresList.setHasFixedSize(true); // doesn't refer to # of items in list
    }

    private void setNewId(long newId) {
        ((ScoreCursorAdapter)dataBindings.scoresList.getAdapter()).setNewScoreId(newId);
        dataBindings.scoresList.scrollToPosition(0);
    }
}
